α = 1.0
N = 999
K = 9
B = 100
stop_at_sol = false
algo = :ce

γ = 1e-1
η = 3e-3
β = 5e-1
β₁ = 1.0002e0
γ₁ = 1.0004e0
epochs = 20_000

loss_threshold = 1e-7
