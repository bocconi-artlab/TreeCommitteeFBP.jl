module DBeta

using Flux
using Flux: glorot_uniform, @treelike, param

export Treeβ, Comm, CommReLU, chain, LAL

mutable struct Treeβ{S,T}
    Ws::S
    N::Integer
    K::Integer
    β::T
end

normW!(W::Vector) = W ./= √sum(W.^2)
function normW!(W::Matrix)
    W ./= .√(vec(sum(W.^2, dims=2)))
end
normW!(Ws::Vector{<:VecOrMat}) = foreach(normW!, Ws)


function Treeβ(N::Integer, K::Integer;
               β = 1f0, initW = (in,_)->Float64.(glorot_uniform(1,in)))
    @assert N % K == 0
    return Treeβ(tuple([param(normW!(initW(N÷K, k))) for k = 1:K]...), N, K, β)
end

@treelike Treeβ

function (a::Treeβ)(x::AbstractArray)
    Ws, N, K, β = a.Ws, a.N, a.K, a.β
    Nk = N ÷ K
    tanh.(β .* vcat((Ws[k] * @view(x[(1:Nk) .+ (k-1)*Nk,:]) for k = 1:K)...))
end

function Base.show(io::IO, l::Treeβ)
    print(io, "Treeβ($(l.N), $(l.K))")
end


mutable struct Comm{S}
    Ws::S
    N::Integer
    K::Integer
    h::AbstractMatrix
    Δ::AbstractVector
end

function Comm(N::Integer, K::Integer;
              initW = (in,_)->Float64.(glorot_uniform(1,in)))
    @assert N % K == 0
    return Comm(tuple([normW!(initW(N÷K, k)) for k = 1:K]...), N, K,
                zeros(K,0), zeros(0))
end

function Comm(t::Treeβ)
    return Comm(tuple([W.data for W in t.Ws]...), t.N, t.K,
                zeros(t.K,0), zeros(0))
end

function Comm(Ws::Vector{<:VecOrMat{<:Real}})
    K = length(Ws)
    N = sum(length.(Ws))
    Nk = N ÷ K
    return Comm(tuple([normW!(reshape(W, 1, Nk)) for W in Ws]...), N, K,
                zeros(K,0), zeros(0))
end

@treelike Comm

Θ(x) = 2.0 .* (x .> 0) .- 1

function (a::Comm)(x::AbstractMatrix)
    Ws, N, K = a.Ws, a.N, a.K
    Nk = N ÷ K
    h = vcat((Ws[k] * @view(x[(1:Nk) .+ (k-1)*Nk,:]) for k = 1:K)...)
    a.h = h
    Δ = vec(sum(Θ(h), dims=1))
    a.Δ = Δ
    σ = Θ(Δ)
    return σ
end

function Base.show(io::IO, l::Comm)
    print(io, "Comm($(l.N), $(l.K))")
end


mutable struct LAL
    η::Float64
end


function chain(c::Comm; β = 20.0)
    Ws, N, K = c.Ws, c.N, c.K
    return Chain(
        Treeβ(N, K; β = β, initW = (_,k)->copy(Ws[k])),
        x->vec(sum(x, dims=1) ./ √K)
       )
end




mutable struct CommReLU{S}
    Ws::S
    N::Integer
    K::Integer
    h::AbstractMatrix
    Δ::AbstractVector
    c::AbstractVector
    function CommReLU(Ws::S, N::Integer, K::Integer, h::AbstractMatrix, Δ::AbstractVector) where S
        @assert N % K == 0
        @assert K % 2 == 0
        c = ones(K)
        c[(K÷2)+1:end] .*= -1
        return new{S}(Ws, N, K, h, Δ, c)
    end
end

function CommReLU(N::Integer, K::Integer;
                  initW = (in,_)->Float64.(glorot_uniform(1,in)))
    @assert N % K == 0
    return CommReLU(tuple([normW!(initW(N÷K, k)) for k = 1:K]...), N, K, # TODO fix normalization
                    zeros(K,0), zeros(0))
end

function CommReLU(t::Treeβ)
    return CommReLU(tuple([W.data for W in t.Ws]...), t.N, t.K,
                    zeros(t.K,0), zeros(0))
end

function CommReLU(Ws::Vector{<:VecOrMat{<:Real}})
    K = length(Ws)
    N = sum(length.(Ws))
    Nk = N ÷ K
    return CommReLU(tuple([normW!(reshape(W, 1, Nk)) for W in Ws]...), N, K,
                    zeros(K,0), zeros(0))
end

@treelike CommReLU

# ReLU(x) = max.(x, 0.0)
ReLU(x) = (y->ifelse(y>0,y,0.0*y)).(x)

function (a::CommReLU)(x::AbstractMatrix)
    Ws, N, K, c = a.Ws, a.N, a.K, a.c
    Nk = N ÷ K
    h = vcat((Ws[k] * @view(x[(1:Nk) .+ (k-1)*Nk,:]) for k = 1:K)...)
    @assert size(h) == (K, size(x,2))
    a.h = h # TODO: imrprove mem managment?
    Δ = vec(c' * ReLU(h))
    a.Δ = Δ
    σ = Θ(Δ)
    return σ
end

function Base.show(io::IO, l::CommReLU)
    print(io, "CommReLU($(l.N), $(l.K))")
end

end # module
