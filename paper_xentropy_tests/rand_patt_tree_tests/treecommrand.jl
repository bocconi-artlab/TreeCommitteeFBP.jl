module TreeCommSGD

using Flux
using Flux: data
using Base.Iterators: partition, repeated, flatten
using Random, Statistics, LinearAlgebra, DelimitedFiles
using ExtractMacro
using Distributed

include("aux.jl")
include("DBeta.jl")
using .DBeta
include("hess.jl")

function renorm!(ms::Chain...)
    for m in ms, Wt in m.layers[1].Ws
        @assert all(iszero, Wt.grad)
        W = Wt.data
        W ./= .√sum(W.^2, dims=2)
    end
end
function renorm!(m::Comm)
    for W in m.Ws
        W ./= .√sum(W.^2, dims=2)
    end
end
function renorm!(m::CommReLU)
    # z = 0.0
    # for W in m.Ws
    #     z += √sum(W.^2)
    # end
    # for W in m.Ws
    #     W ./= (z / m.K)
    # end
    for W in m.Ws
        W ./= .√sum(W.^2, dims=2)
    end
end

function loss_mse(m, x, y, γ)
    return sum((tanh.(m(x) .* γ) .- y).^2)
end

function loss_ce(m, x, y, γ)
    return sum(map(z->ce(z, γ), m(x) .* y))
end

function loss_lal(m::Comm, x, y)
    ŷ = m(x)
    @extract m : Ws N K h
    s = zero(eltype(h))
    for a = 1:length(ŷ)
        ŷ[a] * y[a] > 0 && continue

        δₘ = eltype(h)(-Inf)
        for k = 1:K
            δ = h[k,a] * y[a]
            δ > 0 && continue
            δₘ = max(δₘ, δ)
        end
        s -= δₘ
    end
    return s
end

function loss_lalrelu(m::Comm, x, y)
    ŷ = m(x)
    @extract m : Ws N K h c
    s = zero(eltype(h))
    for a = 1:length(ŷ)
        ŷ[a] * y[a] > 0 && continue

        δₘ = eltype(h)(Inf)
        for k = 1:K
            δ = c[k] * h[k,a] * y[a]
            δ > 0 && h[k,a] < 0 && continue
            δₘ = min(δₘ, abs(δ))
        end
        s += δₘ^2
    end
    return s
end

accuracy(m, x, y) = mean(sign.(m(x)) .== y)
function saccuracy(m, x, y)
    oldβ = m.layers[1].β
    m.layers[1].β = floatmax(oldβ)
    acc = accuracy(m, x, y)
    m.layers[1].β = oldβ
    return acc
end

function callback(ep, m, X, Y, γ; stop_at_sol = false)
    trnacc = data(accuracy(m, X, Y))
    s_trnacc = data(saccuracy(m, X, Y))

    renorm!(m)

    stopflag = stop_at_sol && s_trnacc == 1
    return stopflag, (trnacc, s_trnacc)
end

mutable struct Setup{NN,OT}
    X
    Y
    indata
    labels
    m::NN
    loss::Function
    opt::OT
    γ::Real
    β₁::Real
    γ₁::Real
    eps1::Int
    dir::String
    outfile::String
    epochs::Int
    stop_at_sol::Bool
    loss_threshold::Real
    id::String
end

function runtests(conffile::String, ns; force = false)
    # unsolved = Int[]
    # β0s = Dict{Int,Float64}()
    # id, dir, file_templ = statsdir(conffile)
    # mkpath(dir)

    for n in ns
        @time solved, converged, setup = runtest(conffile, n; force = force)
        # @assert id == setup.id
        # if !solved
        #     push!(unsolved, n)
        # end
        # β0 = round(seek_minβ(setup, tol=1e-2), RoundUp)
        # β0s[n] = β0
    end
    # statsfile = joinpath(dir, "stats.$file_templ.txt")
    # open(statsfile, "w") do f
    #     print(f,
    #           """
    #           ns = $ns
    #           unsolved = $unsolved
    #           solved_frac = $(1 - length(unsolved) / length(ns))
    #           β0s = $(dprint(β0s))
    #           """)
    # end
    allstats(conffile, ns)
end

function allstats(conffile::String, ns; statsdir = statsdir)
    id, dir, file_templ = statsdir(conffile)
    statsfile = joinpath(dir, "stats.$file_templ.txt")
    tst_basedir = replace(dir, "stats" => "tests")
    isdir(tst_basedir) || error("tests dir not found: $tst_basedir")
    mkpath(dir)

    unsolved = Int[]
    β0s = Dict{Int,Float64}()
    Δs = Float64[]

    for n in ns
        tst_dir1 = replace(file_templ, ".id_" => ".n_$n.id_") # XXX horrible hack!
        tst_dir = joinpath(tst_basedir, tst_dir1)

        Xfile = joinpath(tst_dir, "X.txt")
        X = Matrix(readdlm(Xfile)')
        # Nk, M = size(X)
        # Yfile = joinpath(tst_dir, "Y.txt")
        # Y = vec(readdlm(Yfile))
        Y = ones(size(X, 2))

        Wmat = readdlm(joinpath(tst_dir, "W.txt"))
        Ws = [Wmat[k:k,:] for k = 1:size(Wmat,1)]

        m = Comm(Ws)
        acc = accuracy(m, X, Y)
        @info "acc = $acc"

        solved = acc == 1
        if !solved
            push!(unsolved, n)
        end
        β0 = round(seek_minβ(X, Y, m, tol=1e-2), RoundUp)
        β0s[n] = β0

        sf = joinpath(tst_dir, "stab.txt")
        if isfile(sf)
            Δ = readdlm(sf)
            append!(Δs, Δ)
        end
    end
    statsfile = joinpath(dir, "stats.$file_templ.txt")
    open(statsfile, "w") do f
        print(f,
              """
              ns = $ns
              unsolved = $unsolved
              solved_frac = $(1 - length(unsolved) / length(ns))
              β0s = $(dprint(β0s))
              """)
    end
    if !isempty(Δs)
        writedlm(joinpath(dir, "stabs.$file_templ.txt"), Δs)
    end
end

function statshess(conffile; βs = [1.0:5.0; 10.0:5.0:50.0], onlysolved = true, statsdir = statsdir)
    id, dir, file_templ = statsdir(conffile)
    statsfile = joinpath(dir, "stats.$file_templ.txt")
    isfile(statsfile) || error("stats file not found: $statsfile")
    tst_basedir = replace(dir, "stats" => "tests")
    isdir(tst_basedir) || error("tests dir not found: $tst_basedir")

    allevs = Dict{Float64,Vector{Float64}}()
    maxevs = Dict{Float64,Dict{Int,Float64}}()

    ns, unsolved, β0s = open(statsfile, "r") do f
        l = readline(f)
        startswith(l, "ns = ") || error("invalid ns line: $l")
        ns = eval(Meta.parse(replace(l, "ns = "=>"")))::AbstractVector
        l = readline(f)
        startswith(l, "unsolved = ") || error("invlide unsolved line: $l")
        unsolved = eval(Meta.parse(replace(l, "unsolved = " => "")))::Vector{Int}
        l = readline(f)
        startswith(l, "solved_frac = ") || error("unexpected line: $l")
        l = readline(f)
        startswith(l, "β0s = ") || error("invalid β0s line: $l")
        β0s = eval(Meta.parse("Dict{Int,Float64}(" * replace(l, "β0s = " => "") * ")"))::Dict{Int,Float64}
        return ns, unsolved, β0s
    end

    # for n in ns
    p_evs = pmap(ns) do n
        allevs = Dict{Float64,Vector{Float64}}()
        maxevs = Dict{Float64,Dict{Int,Float64}}()
        onlysolved && ns ∈ unsolved && return allevs, maxevs
        tst_dir = joinpath(tst_basedir, replace(file_templ, ".id" => ".n_$n.id")) # XXX horrible hack
        pattfile = joinpath(tst_dir, "X.txt")
        X = Matrix(readdlm(pattfile)')
        N, M = size(X)
        Y = ones(M)
        Wmat = readdlm(joinpath(tst_dir, "W.txt"))
        Ws = [Wmat[k:k,:] for k = 1:size(Wmat,1)]

        β0 = β0s[n]
        for β in βs
            β ≥ β0 || continue
            @info "computing Hessian for β = $β"
            @time H, ev = comp_hessian(X, Y, Ws, tst_dir, β = β, correction=false);
            maxev = maximum(ev)
            @info "  max ev = $maxev"
            haskey(allevs, β) || (allevs[β] = Float64[])
            haskey(maxevs, β) || (maxevs[β] = Dict{Int,Float64}())
            append!(allevs[β], ev)
            maxevs[β][n] = maxev
        end
        return allevs, maxevs
    end

    for (pall, pmax) in p_evs
        for (β, evs) in pall
            haskey(allevs, β) || (allevs[β] = Float64[])
            append!(allevs[β], evs)
        end
        for (β, devs) in pmax
            haskey(maxevs, β) || (maxevs[β] = Dict{Int,Float64}())
            for (n,maxev) in devs
                maxevs[β][n] = maxev
            end
        end
    end

    for (β,ev) in allevs
        sort!(ev, rev=true)
        evfilen = joinpath(dir, "all_eigvals.$file_templ.beta_$β.gamma_1.0.corr_false.os_$onlysolved.txt")
        writedlm(evfilen, ev)
    end
    for (β,ev) in maxevs
        evfilen = joinpath(dir, "max_eigvals.$file_templ.beta_$β.gamma_1.0.corr_false.os_$onlysolved.txt")
        open(evfilen, "w") do f
            println(f, dprint(ev))
        end
    end
end




function statsdir(conffile::String)
    id = @getsettings conffile begin
        α,              Float64
        N,              Int
        K,              Int
        B,              Int
        stop_at_sol,    Bool
        algo,           Symbol

        γ,              Float64
        η,              Float64
        β,              Float64
        β₁,             Float64
        γ₁,             Float64

        epochs,         Int
        loss_threshold, Float64
    end

    @assert N % K == 0
    if algo ∈ [:lal, :lalrelu]
        @assert γ == 0
        @assert β == 0
        @assert γ₁ == 0
        @assert β₁ == 0
        @assert stop_at_sol
    end
    if algo == :lalrelu
        @assert K % 2 == 0
    end

    dir = "stats.N_$(N).K_$(K).alpha_$(α)"
    file_templ = "algo_$(algo).stop_$(stop_at_sol).id_$id"
    return id, dir, file_templ
end


function runtest(conffile::String, n::Int; force = false)

    id = @getsettings conffile begin
        α,              Float64
        N,              Int
        K,              Int
        B,              Int
        stop_at_sol,    Bool
        algo,           Symbol

        γ,              Float64
        η,              Float64
        β,              Float64
        β₁,             Float64
        γ₁,             Float64

        epochs,         Int
        loss_threshold, Float64
    end

    @info "ID = $id"
    N % K == 0 || throw(ArgumentError("invalid K: N=$N K=$K"))

    loss = algo == :ce      ? loss_ce      :
           algo == :mse     ? loss_mse     :
           algo == :lal     ? loss_lal     :
           algo == :lalrelu ? loss_lalrelu :
           error("unknown algorithm $algo")

    if algo ∈ [:lal, :lalrelu]
        @assert γ == 0
        @assert β == 0
        @assert γ₁ == 0
        @assert β₁ == 0
        @assert stop_at_sol
    end
    if algo == :lalrelu
        @assert K % 2 == 0
    end

    M = B * round(Int, (α * N) / B)
    abs(M - round(Int, α * N)) > 1 && @warn "dicrepancy! M=$M αN=$(round(Int, α * N)) N=$N α=$α B=$B"

    dir = joinpath("tests.N_$N.K_$K.alpha_$α", "algo_$algo.stop_$stop_at_sol.n_$n.id_$id")
    outcf = joinpath(dir, "conf.jl")
    outfile = joinpath(dir, "log.txt")
    if isfile(outcf) && read(outcf) ≠ read(conffile)
        error("possible id clash, bailing out: dir=$dir")
    end
    if isfile(outfile)
        force || error("outfile exists, use force=true to overwrite ($outfile)")
    else
        mkpath(dir)
    end
    cp(conffile, outcf, force = true)

    X = randpatt(N, M, n)
    pattfile = joinpath(dir, "X.txt")
    writedlm(pattfile, X')

    if algo == :lalrelu
        Y = 2 .* (rand(M) .> 0.5) .- 1
    else
        Y = ones(M)
    end

    indata = [X[:, (1:B) .+ (b-1)*B] for b = 1:(M÷B)]
    labels = [Y[(1:B) .+ (b-1)*B] for b = 1:(M÷B)]

    if algo == :lal
        m = Comm(N, K)
        opt = LAL(η)
    elseif algo == :lalrelu
        m = CommReLU(N, K)
        opt = LAL(η)
    else
        m = Chain(
            Treeβ(N, K, β = β),
            x->vec(sum(x, dims=1) ./ √K)
           )
        opt = Descent(η)
    end

    @show accuracy(m, X, Y)

    eps1 = 1

    open(outfile, "w") do f
        println(f, "# η=$η β₀=$β γ₀=$γ β₁=$β₁ γ₁=$γ₁")
    end

    setup = Setup(X, Y, indata, labels, m, loss, opt,
                  γ, β₁, γ₁, eps1, dir, outfile, epochs,
                  stop_at_sol, loss_threshold, id)

    return run!(setup)
end

function run!(setup::Setup; st0 = 0)
    @extract setup : X Y indata labels m loss opt γ β₁ γ₁ eps1 dir outfile epochs stop_at_sol loss_threshold

    converged = false
    solved = false
    for st = (1:epochs) .+ st0
        @info "γ=$γ β=$(m.layers[1].β)"
        renorm!(m)

        result = etrain!((eps1,1),
                         (x,y)->loss(m, x, y, γ),
                         params(m), (indata, labels),
                         opt,
                         ep0 = (st-1) * eps1,
                         tstcallback = ep->callback(ep, m, X, Y, γ, stop_at_sol=stop_at_sol),
                         outf = outfile,
                         loss_threshold = loss_threshold)
        m.layers[1].β *= β₁
        γ *= γ₁
        if result == :converged
            @info "CONVERGED"
            converged = true
            break
        elseif result == :stopped
            break
        end
    end
    _, (trnacc, s_trnacc) = callback(0, m, X, Y, γ)
    solved = s_trnacc == 1
    @info "acc = $trnacc s_acc = $s_trnacc"
    solved && @info "SOLVED"

    dumpweights(m, dir)

    return solved, converged, setup
end

function run!(setup::Setup{<:Comm,LAL}; st0 = 0)
    @extract setup : X Y indata labels m loss opt eps1 dir outfile epochs loss_threshold

    @assert loss == loss_lal
    @assert eps1 == 1

    @extract m : Ws N K
    Nk = N ÷ K
    r(k) = (1:Nk) .+ (k-1)*Nk

    solved = false
    converged = false
    for st = (1:epochs) .+ st0
        renorm!(m)
        l = 0.0
        errs = 0
        M = 0
        for (x, y) in zip(indata, labels)
            B = length(y)
            M += B
            σ = m(x)
            h = m.h
            for a = 1:B
                σ[a] * y[a] > 0 && continue
                errs += 1

                kₘ = 0
                δₘ = eltype(h)(-Inf)
                for k = 1:K
                    δ = h[k,a] * y[a]
                    δ > 0 && continue
                    if δ > δₘ
                        kₘ = k
                        δₘ = δ
                    end
                end
                l -= δₘ
                Ws[kₘ] .+= sum((opt.η * y[a]) .* @view(x[r(kₘ),a]), dims=2)'
            end
        end
        l /= M
        acc = 1 - errs / M
        outstr = "  epoch $st: loss=$l ($acc)"
        println(outstr)
        outfile ≠ nothing && open(f->println(f, outstr), outfile, "a")

        if l ≤ loss_threshold
            @info "CONVERGED"
            converged = true
            break
        elseif acc == 1
            break
        end
    end
    acc = accuracy(m, X, Y)
    solved = acc == 1
    @info "acc = $acc"
    solved && @info "SOLVED"

    dumpweights(m, dir)
    dumpstab(m, X, Y, dir)

    return solved, converged, setup
end

function run!(setup::Setup{<:CommReLU,LAL}; st0 = 0)
    @extract setup : X Y indata labels m loss opt eps1 dir outfile epochs loss_threshold

    @assert loss == loss_lalrelu
    @assert eps1 == 1

    @extract m : Ws N K c
    Nk = N ÷ K
    r(k) = (1:Nk) .+ (k-1)*Nk

    solved = false
    converged = false
    for st = (1:epochs) .+ st0
        renorm!(m)
        l = 0.0
        errs = 0
        M = 0
        for (x, y) in zip(indata, labels)
            B = length(y)
            M += B
            σ = m(x)
            h = m.h
            # meanh = mean(h)
            for a = 1:B
                σ[a] * y[a] > 0 && continue
                errs += 1

                kₘ = 0
                δₘ = eltype(h)(Inf)
                for k = 1:K
                    δ = c[k] * h[k,a] * y[a]
                    δ > 0 && continue
                    h[k,a] < 0 && rand() < 0.99 && continue
                    # h[k,a] < 0 && rand() < 0.9 && continue
                    # δ > 0 && h[k,a] < 0 && continue
                    # δ > 0 && continue
                    if abs(δ) < δₘ
                        kₘ = k
                        δₘ = abs(δ)
                    end
                end
                # if kₘ == 0
                #     for k = 1:K
                #         δ = c[k] * h[k,a] * y[a]
                #         # h[k,a] < 0 && rand() < 0.95 && continue
                #         δ > 0 && continue
                #         # δ > 0 && h[k,a] < 0 && continue
                #         # δ > 0 && continue
                #         if abs(δ) < δₘ
                #             kₘ = k
                #             δₘ = abs(δ)
                #         end
                #     end
                # end
                kₘ == 0 && continue
                l += δₘ
                Ws[kₘ] .+= sum((opt.η * y[a] * c[kₘ]) .* @view(x[r(kₘ),a]), dims=2)'
            end
        end
        l /= M
        acc = 1 - errs / M
        outstr = "  epoch $st: loss=$l ($acc)"
        println(outstr)
        outfile ≠ nothing && open(f->println(f, outstr), outfile, "a")

        if l < loss_threshold
            @info "CONVERGED"
            converged = true
            break
        elseif acc == 1
            break
        end
    end
    acc = accuracy(m, X, Y)
    solved = acc == 1
    @info "acc = $acc"
    solved && @info "SOLVED"

    dumpweights(m, dir)
    dumpstab(m, X, Y, dir)

    return solved, converged, setup
end

function dumpweights(m::Chain, dir::String)
    Wfile = joinpath(dir, "W.txt")
    writedlm(Wfile, vcat((W.data for W in m.layers[1].Ws)...))
end

function dumpweights(m::Union{Comm,CommReLU}, dir::String)
    Wfile = joinpath(dir, "W.txt")
    writedlm(Wfile, vcat(m.Ws...))
end

function comp_hessian(setup::Setup; kw...)
    @extract setup : X Y m dir
    Ws = m isa Comm ? m.Ws : [W.data for W in m.layers[1].Ws]
    return comp_hessian(X, Y, Ws, dir; kw...)
end

function comp_hessian(X, Y, Ws, dir::String; β = 10.0, γ = 1.0, correction = false)
    H = HardcodedHessians.hess_treecomm_mse(Ws, X, Y, β, γ, correction=correction)
    Hfile = joinpath(dir, "hess.beta_$β.gamma_$γ.corr_$correction.txt")
    writedlm(Hfile, H)

    ev = sort!(eigvals(H), rev=true)
    evfile = joinpath(dir, "eigvals.beta_$β.gamma_$γ.corr_$correction.txt")
    writedlm(evfile, ev)

    return H, ev
end

function seek_minβ(setup::Setup; kw...)
    @extract setup: X Y m
    return seek_minβ(X, Y, m; kw...)
end

seek_minβ(X, Y, m::Comm; kw...) = seek_minβ(X, Y, chain(m); kw...)

function seek_minβ(X, Y, m::Chain; tol = 0.5)
    curr_β = m.layers[1].β
    _, (trnacc, s_trnacc) = callback(0, m, X, Y, 1.0)
    # @info "acc = ($trnacc, $s_trnacc)"
    if trnacc ≥ s_trnacc
        β₁ = curr_β
        β₀ = 1e-10
    else
        β₁ = 1e10
        β₀ = curr_β
    end
    δ = β₁ - β₀
    while δ > tol
        βₘ = √(β₁ * β₀)
        # @info "β₉,β₁ = $β₀,$β₁ βₘ = $βₘ"
        m.layers[1].β = βₘ
        trnacc = data(accuracy(m, X, Y))
        # @info "  acc = $trnacc"
        if trnacc ≥ s_trnacc
            β₁ = βₘ
        else
            β₀ = βₘ
        end
        δ = β₁ - β₀
    end
    m.layers[1].β = curr_β
    @info "minβ = $β₁"
    return β₁
end

function dumpstab(m::Union{Comm,CommReLU}, X, Y, dir)
    σ = m(X)
    Δ = m.Δ .* Y
    stabfile = joinpath(dir, "stab.txt")
    writedlm(stabfile, Δ)
end

end # module TreeCommSGD
