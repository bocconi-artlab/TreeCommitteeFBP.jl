module TreeCommSGD_RE

using Flux
using Flux: data
import Flux: params
using Statistics
using DelimitedFiles
using Base.Iterators: partition, repeated, flatten
using ExtractMacro
using Distributed

include("aux.jl")
include("treecommrand.jl")
using .TreeCommSGD.DBeta

import .TreeCommSGD: renorm!

const loss_mse = TreeCommSGD.loss_mse
const loss_ce = TreeCommSGD.loss_ce
const loss_lal = TreeCommSGD.loss_lal

function runtests(conffile::String, ns; force = false)
    pmap(ns) do n
        @time solved, converged, setup = runtest(conffile, n; force = force)
    end
    allstats(conffile, ns)
end

allstats(conffile, ns; kw...) = TreeCommSGD.allstats(conffile, ns; statsdir = statsdir, kw...)
statshess(conffile; kw...) = TreeCommSGD.statshess(conffile; statsdir = statsdir, kw...)

function statsdir(conffile::String)
    id = @getsettings conffile begin
        α,              Float64
        N,              Int
        K,              Int
        B,              Int
        R,              Int
        stop_at_sol,    Bool
        algo,           Symbol

        γ,              Float64
        η,              Float64
        β,              Float64
        β₁,             Float64
        γ₁,             Float64
        ϕ,              Float64
        ϕ₁,             Float64
        η₁,             Float64, 1.0

        epochs,         Int
        loss_threshold, Float64
    end

    @assert N % K == 0
    @assert algo ∈ [:REce, :REmse, :RElal]

    dir = "stats.N_$N.K_$K.alpha_$α"
    file_templ = "algo_$algo.stop_$stop_at_sol.id_$id"
    return id, dir, file_templ
end

mutable struct Setup{NN,OT}
    X
    Y
    B::Integer
    m::NN
    loss1::Function
    opt::OT
    η::Real
    γ::Real
    β₁::Real
    γ₁::Real
    ϕ::Real
    ϕ₁::Real
    η₁::Real
    eps1::Int
    dir::String
    outfile::String
    epochs::Int
    stop_at_sol::Bool
    loss_threshold::Real
    id::String
end

mutable struct RepDBeta
    N::Int
    K::Int
    R::Int
    mc::Chain
    mr::Vector{Chain}
    β::Float64
    function RepDBeta(N, K, R, β)
        mr = [Chain(
                    Treeβ(N, K, β = β),
                    x->vec(sum(x, dims=1) ./ √K)
                   ) for i = 1:R]
        mc = Chain(
                   Treeβ(N, K, β = β, initW = (_,k)->mean(mr[a].layers[1].Ws[k].data for a = 1:R)),
                   x->vec(sum(x, dims=1) ./ √K)
                  )
        return new(N, K, R, mc, mr, β)
    end
end

params(m::RepDBeta) = params(m.mc, m.mr...)

function lossR(m::RepDBeta, x, y, loss1, γ, ϕ)
    @extract m : K R mr mc
    s = sum((rand() < 0.5) * loss1(m1, x1, y1, γ) for (m1, x1, y1) in zip(mr, x, y))
    # a = rand(1:R)
    # (m1, x1, y1) = collect(zip(mr, x, y))[a]
    # s = loss1(m1, x1, y1, γ)
    Wsc = mc.layers[1].Ws
    for a = 1:R
        # rand() < 0.5 || continue
        Wsa = mr[a].layers[1].Ws
        for k = 1:K
            s += (rand() < 0.5) * ϕ * sum((Wsa[k] .- Wsc[k]) .^ 2)
        end
    end
    return s
end

function diststats(m::RepDBeta)
    @extract m : K R mc mr
    Wsc = mc.layers[1].Ws

    dists = Array{Float64}(undef, R)
    dk = Array{Float64}(undef, K)
    for a = 1:R
        Wsa = mr[a].layers[1].Ws
        dk .= 0
        for k = 1:K
            Wc = Wsc[k].data
            Wa = Wsa[k].data
            dk[k] = √sum((Wa .- Wc) .^ 2)
        end
        dists[a] = mean(dk)
    end
    return (mean(dists), extrema(dists)...)
end

function callback(ep, m::RepDBeta, X, Y, γ; stop_at_sol = false)
    trnacc = data(accuracy(m, X, Y))
    s_trnacc = data(saccuracy(m, X, Y))

    renorm!(m)

    stopflag = stop_at_sol && s_trnacc == 1
    return stopflag, (trnacc, s_trnacc, diststats(m)...)
end

accuracy(m::RepDBeta, x, y) = TreeCommSGD.accuracy(m.mc, x, y)
saccuracy(m::RepDBeta, x, y) = TreeCommSGD.saccuracy(m.mc, x, y)

function renorm!(m::RepDBeta)
    for m1 in (m.mc, m.mr...)
        for Wt in m1.layers[1].Ws
            @assert all(iszero, Wt.grad)
            W = Wt.data
            W ./= .√sum(W.^2, dims=2)
        end
    end
    return m
end

function updateβ!(m::RepDBeta, β::Real)
    m.β = β
    for m1 in (m.mc, m.mr...)
        m1.layers[1].β = β
    end
end

mutable struct RepComm
    N::Int
    K::Int
    R::Int
    mc::Comm
    mr::Vector{Comm}
    function RepComm(N, K, R)
        mr = [Comm(N, K) for i = 1:R]
        mc = Comm(N, K, initW = (_,k)->mean(mr[a].Ws[k] for a = 1:R))
        return new(N, K, R, mc, mr)
    end
end

function diststats(m::RepComm)
    @extract m : K R mc mr
    Wsc = mc.Ws

    dists = Array{Float64}(undef, R)
    dk = Array{Float64}(undef, K)
    for a = 1:R
        Wsa = mr[a].Ws
        dk .= 0
        for k = 1:K
            Wc = Wsc[k]
            Wa = Wsa[k]
            dk[k] = √sum((Wa .- Wc) .^ 2)
        end
        dists[a] = mean(dk)
    end
    return (mean(dists), extrema(dists)...)
end

accuracy(m::RepComm, x, y) = TreeCommSGD.accuracy(m.mc, x, y)

function renorm!(m::RepComm)
    for m1 in (m.mc, m.mr...)
        for W in m1.Ws
            W ./= .√sum(W.^2, dims=2)
        end
    end
    return m
end

function runtest(conffile::String, n::Int; force = false)

    id = @getsettings conffile begin
        α,              Float64
        N,              Int
        K,              Int
        B,              Int
        R,              Int
        stop_at_sol,    Bool
        algo,           Symbol

        γ,              Float64
        η,              Float64
        β,              Float64
        β₁,             Float64
        γ₁,             Float64
        ϕ,              Float64
        ϕ₁,             Float64
        η₁,             Float64, 1.0

        epochs,         Int
        loss_threshold, Float64
    end

    @info "ID = $id"
    N % K == 0 || throw(ArgumentError("invalid K: N=$N K=$K"))

    loss1 = algo == :REce  ? loss_ce  :
            algo == :REmse ? loss_mse :
            algo == :RElal ? loss_lal :
            error("unknown algorithm $algo")

    if algo == :lal
        @assert γ == 0
        @assert β == 0
        @assert γ₁ == 0
        @assert β₁ == 0
    end

    M = B * round(Int, (α * N) / B)
    abs(M - round(Int, α * N)) > 1 && @warn "dicrepancy! M=$M αN=$(round(Int, α * N)) N=$N α=$α B=$B"

    dir = joinpath("tests.N_$N.K_$K.alpha_$α", "algo_$algo.stop_$stop_at_sol.n_$n.id_$id")
    outcf = joinpath(dir, "conf.jl")
    outfile = joinpath(dir, "log.txt")
    if isfile(outcf)
        if read(outcf) ≠ read(conffile)
            error("possible id clash, bailing out: dir=$dir")
        end
    end
    if isfile(outfile)
        force || error("outfile exists, use force=true to overwrite ($outfile)")
    else
        mkpath(dir)
    end
    cp(conffile, outcf, force = true)

    X = randpatt(N, M, n)
    pattfile = joinpath(dir, "X.txt")
    writedlm(pattfile, X')

    Y = ones(M)

    if algo == :RElal
        m = RepComm(N, K, R)
        opt = LAL(η)
    else
        m = RepDBeta(N, K, R, β)
        opt = Descent(η)
    end

    @show accuracy(m, X, Y)

    eps1 = 1

    open(outfile, "w") do f
        println(f, "# η=$η β₀=$β γ₀=$γ ϕ₀=$ϕ η₁=$η₁ β₁=$β₁ γ₁=$γ₁ ϕ₁=$ϕ₁")
    end

    setup = Setup(X, Y, B, m, loss1, opt,
                  η, γ, β₁, γ₁, ϕ, ϕ₁, η₁, eps1, dir, outfile, epochs,
                  stop_at_sol, loss_threshold, id)

    return run!(setup)
end

function run!(setup::Setup{RepDBeta}; st0 = 0)
    @extract setup : X Y B m loss1 η γ β₁ γ₁ ϕ ϕ₁ η₁ eps1 dir outfile epochs stop_at_sol loss_threshold
    @extract m : R

    converged = false
    solved = false
    for st = (1:epochs) .+ st0
        @extract setup : η γ ϕ
        @extract m : β
        @info "γ=$γ β=$β ϕ=$ϕ η=$η"
        renorm!(m)
        # recenter!(m)

        opt = Descent(η)
        M = length(Y)
        ps = [randperm(M) for a = 1:R]
        r(b) = (1:B) .+ (b-1)*B
        indata = [(X[:, ps[a][r(b)]] for a = 1:R) for b = 1:(M÷B)]
        labels = [(Y[ps[a][r(b)]] for a = 1:R) for b = 1:(M÷B)]

        result = etrain!((eps1,1),
                         (x,y)->lossR(m, x, y, loss1, γ, ϕ),
                         params(m), (indata, labels),
                         opt,
                         ep0 = (st-1) * eps1,
                         tstcallback = ep->callback(ep, m, X, Y, γ, stop_at_sol=stop_at_sol),
                         outf = outfile,
                         loss_threshold = loss_threshold)
        β *= β₁
        updateβ!(m, β)
        setup.γ *= γ₁
        setup.ϕ *= ϕ₁
        setup.η *= η₁
        if result == :converged
            @info "CONVERGED"
            converged = true
            break
        elseif result == :stopped
            break
        end
    end
    _, (trnacc, s_trnacc) = callback(0, m, X, Y, γ)
    solved = s_trnacc == 1
    @info "acc = $trnacc s_acc = $s_trnacc"
    solved && @info "SOLVED"

    TreeCommSGD.dumpweights(m.mc, dir)

    return solved, converged, setup
end

function run!(setup::Setup{RepComm,LAL}; st0 = 0)
    @extract setup : X Y B m loss1 η γ β₁ γ₁ ϕ ϕ₁ η₁ eps1 dir outfile epochs stop_at_sol loss_threshold
    @extract m : R

    @assert loss1 == loss_lal
    @assert eps1 == 1

    @extract m : N K
    Nk = N ÷ K
    rr(k) = (1:Nk) .+ (k-1)*Nk

    converged = false
    solved = false
    for st = (1:epochs) .+ st0
        @extract setup : ϕ
        @info "ϕ=$ϕ, η=$η"
        renorm!(m)

        M = length(Y)
        ps = [randperm(M) for a = 1:R]
        r(b) = (1:B) .+ (b-1)*B
        indata = [(X[:, ps[a][r(b)]] for a = 1:R) for b = 1:(M÷B)]
        labels = [(Y[ps[a][r(b)]] for a = 1:R) for b = 1:(M÷B)]

        l = 0.0
        for (xb, yb) in zip(indata, labels)
            for (a,x,y) in zip(1:R, xb, yb)
                ma = m.mr[a]
                mc = m.mc
                Ws = ma.Ws
                @assert length(y) == B
                σ = ma(x)
                h = ma.h
                for b = 1:B
                    σ[b] * y[b] > 0 && continue

                    kₘ = 0
                    δₘ = eltype(h)(-Inf)
                    for k = 1:K
                        δ = h[k,b] * y[b]
                        δ > 0 && continue
                        if δ > δₘ
                            kₘ = k
                            δₘ = δ
                        end
                    end
                    l -= δₘ
                    Ws[kₘ] .+= sum((η * y[b]) .* @view(x[rr(kₘ),b]), dims=2)'
                end
                for k in 1:K
                    d = (mc.Ws[k] - Ws[k])
                    Ws[k] .+= η .* ϕ .* d
                    mc.Ws[k] .-= η .* ϕ .* d
                    l += ϕ * sum(d.^2)
                end
            end
        end
        acc = accuracy(m, X, Y)
        outstr = "  epoch $st: loss=$l ($acc) $(diststats(m))"
        println(outstr)
        outfile ≠ nothing && open(f->println(f, outstr), outfile, "a")

        setup.ϕ *= ϕ₁
        setup.η *= η₁
        if l ≤ loss_threshold
            @info "CONVERGED"
            converged = true
            break
        elseif stop_at_sol && acc == 1
            break
        end
    end
    acc = accuracy(m, X, Y)
    solved = acc == 1
    @info "acc = $acc"
    solved && @info "SOLVED"

    renorm!(m)
    TreeCommSGD.dumpweights(m.mc, dir)

    return solved, converged, setup
end

end # module TreeCommSGD_R

