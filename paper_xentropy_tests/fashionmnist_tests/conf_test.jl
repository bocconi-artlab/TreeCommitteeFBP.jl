M = 500
classes = (3, 4)
K = 9
B = 100
stop_at_sol = false
algo = :ce

γ = 5e-1
η = 3e-5
β = 5e-1
β₁ = 1.001e0
γ₁ = 1.001e0
epochs = 20_000

loss_threshold = 1e-7
