module PerturbInputs

using Statistics
using DelimitedFiles
using ExtractMacro
using Distributed

include("aux.jl")

include("fullcommfmnist.jl")
include("fullcommfmnist_RE.jl")
const SEEDS = FullCommSGD_FMNIST.SEEDS
using .FullCommSGD_FMNIST.DBeta

function parseW(filename::AbstractString)
    wm = readdlm(filename)
    K, Nk = size(wm)
    N = K * Nk
    W = [wm[k,:] for k = 1:K]
    return N, K, W
end

function run_perturb(conffile::String, n::SEEDS, algo::Symbol;
                     ηs::AbstractVector{<:Float64} = 0.0:0.01:0.3,
                     samples::Integer = 200,
                     seed::Integer = 8723742,
                     ifexists::Symbol = :skip
                    )
    ifexists ∈ [:overwrite, :skip, :error] || throw(ArgumentError("ifexists must be either :overwrite, :skip or :error; given: $ifexists"))
    algo ∈ [:ce, :mse, :lal, :fBP, :REce, :REmse, :RElal] || throw(ArgumentError("unknown algorithm: $algo"))

    if algo == :fBP
        id = @getsettings conffile begin
            M,           Int
            classes,     Tuple{Int,Int},         (2, 4)
            lim,         Tuple{Float64,Float64}, (0.25, 0.75)
            K,           Int
            stop_at_sol, Bool

            max_iters,   Int,              200
            max_steps,   Int,              100
            seed,        Int,              678876
            damping,     Float64,          0.0
            accuracy1,   Symbol,           :accurate
            scoping,     Vector,           [(γ,10.0) for γ in exprange(0.5, 50.0, 30)]
            ϵ,           Float64,          1e-2
            rand_fact,   Float64,          0.0
            κ,           Int,              0

            randomization,  Symbol, :none, nodefhash
        end

        @info "ID = $id"

        N % K == 0 || throw(ArgumentError("invalid K: N=$N K=$K"))
        fprotocol = FreeScoping(scoping)
        α = round(M / N, digits=1) # NOT NICE!
    elseif algo ∈ [:REce, :REmse, :RElal]
        id = @getsettings conffile begin
            M,              Int
            classes,        Tuple{Int,Int},         (2, 4)
            lim,            Tuple{Float64,Float64}, (0.25, 0.75)
            K,              Int
            B,              Int
            R,              Int
            stop_at_sol,    Bool
            algo,           Symbol

            γ,              Float64
            η,              Float64
            β,              Float64
            β₁,             Float64
            γ₁,             Float64
            ϕ,              Float64
            ϕ₁,             Float64

            epochs,         Int
            loss_threshold, Float64

            randomization,  Symbol, :none, nodefhash
        end

        @info "ID = $id"

        if algo == :RElal
            @assert γ == 0
            @assert β == 0
            @assert γ₁ == 0
            @assert β₁ == 0
        end

        @assert M % B == 0
    else
        id = @getsettings conffile begin
            M,              Int
            classes,        Tuple{Int,Int},         (2, 4)
            lim,            Tuple{Float64,Float64}, (0.25, 0.75)
            K,              Int
            B,              Int
            stop_at_sol,    Bool
            algo,           Symbol

            γ,              Float64
            η,              Float64
            β,              Float64
            β₁,             Float64
            γ₁,             Float64

            epochs,         Int
            loss_threshold, Float64

            randomization,  Symbol, :none, nodefhash
        end

        @info "ID = $id"

        if algo == :lal
            @assert γ == 0
            @assert β == 0
            @assert γ₁ == 0
            @assert β₁ == 0
            @assert stop_at_sol
        end
    end

    if length(n) == 2
        nn, seed = n
        rseed = 0
    else
        nn, rseed, seed = n
    end
    rtag = randomization == :none ? "" : ".rndz_$randomization"
    rtag2 = randomization == :none ? "" : ".rseed_$rseed"
    dir = joinpath("tests.cl_$(classes[1])-$(classes[2]).K_$K.M_$M" * rtag, "algo_$algo.stop_$stop_at_sol.n_$nn" * rtag2 * ".seed_$seed.id_$id")
    isdir(dir) || error("directory not found: $dir")
    Xfile = joinpath(dir, "X.txt")
    X = Matrix(readdlm(Xfile)')
    Yfile = joinpath(dir, "Y.txt")
    Y = vec(readdlm(Yfile))

    solfile = joinpath(dir, "W.txt")
    isfile(solfile) || throw(ArgumentError("file not found: $solfile"))
    N, K, W = parseW(solfile)
    Nk = N ÷ K
    Nk == size(X, 1) || throw(ArgumentError("incompatible patterns and solutions, Nsol=$Nk Npatt=$(size(X,1))"))
    M = size(X, 2)
    @assert length(Y) == M

    comm = Comm(W)

    outfile = joinpath(dir, "perturb.txt")
    if isfile(outfile)
        if ifexists == :error
            throw(ArgumentError("out file exists, use force=true to overwrite: $outfile"))
        elseif ifexists == :skip
            @info "  outfile exists, skipping"
            return
        end
        rm(outfile)
    end

    bias = vec(mean((X .+ 1) ./ 2, dims=2))
    for η in ηs
        f = round(Int, Nk * η)
        macc = 0.0
        macc2 = 0.0
        for s = 1:samples
            Xη = copy(X)
            for a = 1:M
                inds = randperm(Nk)[1:f]
                newx = 2 .* (rand(f) .< bias[inds]) .- 1
                Xη[inds, a] .= newx
            end
            acc = FullCommSGD_FMNIST.accuracy(comm, Xη, Y)
            macc += acc
            macc2 += acc^2
        end
        macc /= samples
        macc2 /= samples
        open(outfile, "a") do f; println(f, "$η $macc $(√(macc2 - macc^2)) $samples"); end
    end

    return
end

function run_all_perturb(conffile::String, ns, algo::Symbol; kw...)
    pmap(ns) do n
        @info "n = $n"
        run_perturb(conffile, n, algo; kw...)
    end
end

function average_perturb(conffile::String, algo::Symbol; onlysolved = true)
    algo ∈ [:ce, :mse, :lal, :fBP, :REce, :REmse, :RElal] || throw(ArgumentError("unknown algorithm: $algo"))

    if algo == :fBP
        id, dir, file_templ = statsdir(conffile)
    elseif algo ∈ [:REce, :REmse, :RElal]
        id, dir, file_templ = FullCommSGD_FMNIST_RE.statsdir(conffile)
    else
        id, dir, file_templ = FullCommSGD_FMNIST.statsdir(conffile)
    end
    statsfile = joinpath(dir, "stats.$file_templ.txt")
    isfile(statsfile) || error("stats file not found: $statsfile")
    tst_basedir = replace(dir, "stats" => "tests")
    isdir(tst_basedir) || error("tests dir not found: $tst_basedir")

    ns, unsolved = open(statsfile, "r") do f
        l = readline(f)
        startswith(l, "ns = ") || error("invalid ns line: $l")
        ns = eval(Meta.parse(replace(l, "ns = "=>"")))::AbstractVector
        l = readline(f)
        startswith(l, "unsolved = ") || error("invlide unsolved line: $l")
        unsolved = eval(Meta.parse(replace(l, "unsolved = " => "")))::Vector{<:SEEDS}
        return ns, unsolved
    end

    stats_macc = Dict{Float64,Vector{Float64}}()
    stats_sacc = Dict{Float64,Vector{Float64}}()
    stats_samples = Dict{Float64,Vector{Int}}()
    outfile = joinpath(dir, "perturb_averages.$file_templ.txt")

    for n in ns
        onlysolved && ns ∈ unsolved && continue
        if length(n) == 2
            tst_dir1 = replace(file_templ, ".id_" => ".n_$(n[1]).seed_$(n[2]).id_") # XXX horrible hack!
        else
            tst_dir1 = replace(file_templ, ".id_" => ".n_$(n[1]).rseed_$(n[2]).seed_$(n[3]).id_") # XXX horrible hack!
        end
        tst_dir = joinpath(tst_basedir, tst_dir1)
        infile = joinpath(tst_dir, "perturb.txt")
        isfile(infile) || (@info "  SKIPPING (FILE NOT FOUND: $infile)"; continue)
        @info "file $infile"
        open(infile) do f
            # l = readline(f)
            # @assert startswith(l, "#")
            for l in readlines(f)
                ls = split(l)
                η, macc, sacc = map(x->parse(Float64, x), ls[1:3])
                samples = parse(Int, ls[4])
                vm = get!(stats_macc, η, Float64[])
                vs = get!(stats_sacc, η, Float64[])
                vn = get!(stats_samples, η, Int[])
                push!(vm, macc)
                push!(vs, sacc)
                push!(vn, samples)
            end
        end
    end
    outd = Dict{Float64,Tuple{Float64,Float64,Int}}()
    for η in keys(stats_macc)
        m = sum(stats_macc[η] .* stats_samples[η])
        m2 = sum((stats_sacc[η].^2 .+ stats_macc[η].^2) .* stats_samples[η])
        samples = sum(stats_samples[η])
        m /= samples
        m2 /= samples
        outd[η] = (m, √(m2 - m^2), samples)
    end
    open(outfile, "w") do f
        println(f, "#η ⟨acc⟩ (std)")
        for η in sort!(collect(keys(outd)))
            println(f, "$η ", join(map(string, outd[η]), " "))
        end
    end
end

end # module

