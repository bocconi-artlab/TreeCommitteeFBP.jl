module HardcodedHessians

using LinearAlgebra
using Random
using ExtractMacro

function getR(w::Vector{T}; runs = 2) where {T<:AbstractFloat}
    n = length(w)
    ŵ = w / norm(w)

    v = Array{Vector{T}}(undef, n)
    v[1] = ŵ

    Random.seed!(5)
    for j = 2:n
        a = randn(T, n)
        for r = 1:runs
            for k = (j-1):-1:1
                a .-= (v[k] ⋅ a) * v[k]
            end
            a ./= norm(a)
        end
        v[j] = a
    end

    R = vcat(v'...)

    # @show maximum(abs.(R * w - [norm(w); zeros(n-1)]))
    # @show maximum(abs.(R'R - I))
    @assert maximum(abs.(R * w - [norm(w); zeros(T, n-1)])) < 20 * eps(T)
    @assert maximum(abs.(R'R - I)) < 20 * eps(T)

    return R
end

struct FwdTreecommMSE
    N::Int
    K::Int
    M::Int
    Nk::Int
    βN::Real
    γK::Real
    r::Function
    x::Matrix
    Δ::Vector
    tΔ::Vector
    S::Vector
    tS::Vector
    δ::Vector
    function FwdTreecommMSE(W, x, y, β, γ)
        K = length(W)
        Nk, M = size(x)
        N = Nk * K
        @assert sum(length.(W)) == N
        r(k) = (1:Nk) .+ (k-1)*Nk

        γK = γ/√K
        βN = β/√Nk

        nW = norm.(W)

        Δ = [(√Nk / nW[k]) .* vec(W[k] * x) for k = 1:K]              # k vecs of sz M

        tΔ = map(Δk->tanh.(βN * Δk), Δ)                   # k vecs of sz M

        S = sum(tΔ)                                      # vec of sz M

        tS = map(Sμ->tanh(γK * Sμ), S)                   # vec of sz M

        δ = tS .- y                                      # vec of sz M

        return new(N, K, M, Nk, βN, γK, r, x, Δ, tΔ, S, tS, δ)
    end
end

struct BckTreecommMSE
    fwd::FwdTreecommMSE
    tΔ1::Vector
    tS1::Vector
    tΔ2::Vector
    tS2::Vector
    ∂S::Vector
    ∂tS::Vector
    function BckTreecommMSE(fwd::FwdTreecommMSE)
        @extract fwd: K βN γK x tΔ tS

        tΔ1 = map(ϕ->βN .* (1 .- ϕ.^2), tΔ)              # k vecs of sz M
        tS1 = map(ϕ->γK * (1 - ϕ^2), tS)                 # vec of sz M
        tΔ2 = map(ϕ->-2βN^2 .* (1 .- ϕ.^2) .* ϕ, tΔ)     # k vecs of sz M
        tS2 = map(ϕ->-2γK^2 * (1 - ϕ^2) * ϕ, tS)         # vec of sz M
        ∂S = [tΔ1[k]' .* x for k = 1:K]                  # k mats of sz Nk × M
        ∂tS = [tS1' .* ∂S[k] for k = 1:K]                # k mats of sz Nk × M

        return new(fwd, tΔ1, tS1, tΔ2, tS2, ∂S, ∂tS)
    end
end

BckTreecommMSE(W, x, y, β, γ) = BckTreecommMSE(FwdTreecommMSE(W, x, y, β, γ))

struct Bck2TreecommMSE
    fwd::FwdTreecommMSE
    tΔ2::Vector
    tS2::Vector
    function Bck2TreecommMSE(fwd::FwdTreecommMSE)
        @extract fwd: βN γK tΔ tS δ

        tΔ2 = map(ϕ->-2βN^2 .* (1 .- ϕ.^2) .* ϕ, tΔ)      # k vecs of sz M
        tS2 = map(ϕ->-2γK^2 * (1 - ϕ^2) * ϕ, tS)         # vec of sz M

        return new(fwd, tΔ2, tS2)
    end
end

Bck2TreecommMSE(W, x, y, β, γ) = Bck2TreecommMSE(FwdTreecommMSE(W, x, y, β, γ))

loss_treecomm_mse(W, x, y, β, γ) = loss_treecomm_mse(FwdTreecommMSE(W, x, y, β, γ))

function loss_treecomm_mse(fwd::FwdTreecommMSE)
    @extract fwd : M δ
    return sum(δ.^2) #/ M
end

grad_treecomm_mse(W, x, y, β, γ) = grad_treecomm_mse(BckTreecommMSE(W, x, y, β, γ))

function grad_treecomm_mse(bck)
    @extract bck: fwd ∂tS
    @extract fwd: N K M r δ

    ∂₁L = zeros(N)
    for k = 1:K
        ∂₁L[r(k)] .+=
            dropdims(sum(2 .* reshape(δ, (1, M)) .* ∂tS[k], dims=2), dims=2) # vec of sz Nk
    end
    # ∂₁L ./= M
    return ∂₁L
end

function hess_treecomm_mse(W, x, y, β, γ; correction=true)
    fwd = FwdTreecommMSE(W, x, y, β, γ)
    bck = BckTreecommMSE(fwd)
    bck2 = Bck2TreecommMSE(fwd)
    h = hess_treecomm_mse(bck, bck2)
    return correction ? hess_correction(grad_treecomm_mse(bck), h, W) : h
end

function hess_treecomm_mse(bck, bck2)
    @extract bck: fwd tS1 ∂S ∂tS
    @extract bck2: tΔ2 tS2
    @extract fwd: N K M Nk r x δ

    ∂₂L = zeros(N, N)
    for k1 = 1:K, k2 = 1:K
        # @info "k1,k2 = $k1,$k2"

        # ∂tSk1 = ∂tS[k1]
        # ∂tSk2 = ∂tS[k2]
        # ∂Sk1 = ∂S[k1]
        # ∂Sk2 = ∂S[k2]
        #
        # for i = 1:Nk, j = 1:Nk
        #     ii, jj = r(k1)[i], r(k2)[j]
        #     for μ = 1:M
        #         ∂₂L[ii, jj] += 2 * ∂tSk1[i,μ] * ∂tSk2[j,μ] +
        #                        2 * δ[μ] * tS2[μ] * ∂Sk1[i,μ] * ∂Sk2[j,μ]
        #     end
        # end

        ∂₂L[r(k1),r(k2)] .+=
            dropdims(sum(2 .* reshape(∂tS[k1], (Nk, 1, M)) .* reshape(∂tS[k2], (1, Nk, M)), dims=3), dims=3) # mat of sz Nk × Nk

        ∂₂L[r(k1),r(k2)] .+=
            dropdims(sum(2 .*
                reshape(δ, (1, 1, M)) .* reshape(tS2, (1, 1, M)) .*
                reshape(∂S[k1], (Nk, 1, M)) .* reshape(∂S[k2], (1, Nk, M)), dims=3), dims=3)        # mat of sz Nk × Nk

    end
    for k = 1:K
        # @info "k = $k"
        # tΔ2k = tΔ2[k]
        # for i = 1:Nk, j = 1:Nk
        #     ii, jj = r(k)[i], r(k)[j]
        #     for μ = 1:M
        #         ∂₂L[ii, jj] += 2 * δ[μ] * tS1[μ] * tΔ2k[μ] * x[i,μ] * xvk[j,μ]
        #     end
        # end
        ∂₂L[r(k),r(k)] .+=
            dropdims(sum(2 .*
                reshape(δ, (1, 1, M)) .* reshape(tS1, (1, 1, M)) .*
                reshape(tΔ2[k], (1, 1, M)) .*
                reshape(x, (Nk, 1, M)) .* reshape(x, (1, Nk, M)), dims=3), dims=3)          # mat of sz Nk × Nk
    end
    # ∂₂L ./= M

    return Symmetric(∂₂L)
end

# sphaerical constratint correction
function hess_correction(∂₁L, ∂₂L, W)
    K = length(W)
    N = sum(length.(W))
    @assert length(∂₁L) == N
    @assert size(∂₂L) == (N, N)
    @assert N % K == 0
    Nk = N ÷ K
    r(k) = (1:Nk) .+ (k-1)*Nk

    R = zeros(N, N)
    for k = 1:K
        R[r(k),r(k)] = getR(vec(W[k]))
    end

    ∂′₂L = R * ∂₂L * R'
    for k = 1:K
        Wk = vec(W[k])
        Wk *= √Nk / norm(Wk)
        rd = diagind(∂′₂L)[r(k)]
        ∂′₂L[rd] .-= (Wk ⋅ ∂₁L[r(k)]) / (Wk ⋅ Wk)
    end

    # throw away the first component for each k
    rr = vcat((r(k)[2:end] for k = 1:K)...)
    ∂′₂L = ∂′₂L[rr, rr]

    return Symmetric(∂′₂L)
end

end # module H
