module DBeta

using Flux
using Flux: glorot_uniform, @treelike, param

export Fullβ, Comm, chain, LAL

mutable struct Fullβ{S,T}
    Ws::S
    Nk::Integer
    K::Integer
    β::T
end

normW!(W::Vector) = W ./= √sum(W.^2)
function normW!(W::Matrix)
    W ./= .√(vec(sum(W.^2, dims=2)))
end
normW!(Ws::Vector{<:VecOrMat}) = foreach(normW!, Ws)


function Fullβ(Nk::Integer, K::Integer;
               β = 1f0, initW = (in,_)->Float64.(glorot_uniform(1,in)))
    return Fullβ(tuple([param(normW!(initW(Nk, k))) for k = 1:K]...), Nk, K, β)
end

@treelike Fullβ

function (a::Fullβ)(x::AbstractArray)
    Ws, K, β = a.Ws, a.K, a.β
    tanh.(β .* vcat(((Ws[k] * x) for k = 1:K)...))
end

function Base.show(io::IO, l::Fullβ)
    print(io, "Fullβ($(l.Nk), $(l.K))")
end


mutable struct Comm{S}
    Ws::S
    Nk::Integer
    K::Integer
    h::AbstractMatrix
end

function Comm(Nk::Integer, K::Integer;
              initW = (in,_)->Float64.(glorot_uniform(1,in)))
    return Comm(tuple([normW!(initW(Nk, k)) for k = 1:K]...), Nk, K,
                zeros(K,0))
end

function Comm(t::Fullβ)
    return Comm(tuple([W.data for W in t.Ws]...), t.Nk, t.K,
                zeros(t.K,0))
end

function Comm(Ws::Vector{<:Array{<:Real}})
    K = length(Ws)
    N = sum(length.(Ws))
    @assert N % K == 0
    Nk = N ÷ K
    return Comm(tuple([normW!(reshape(W, 1, Nk)) for W in Ws]...), Nk, K,
                zeros(K,0))
end

@treelike Comm

Θ(x) = 2.0 .* (x .> 0) .- 1

function (a::Comm)(x::AbstractMatrix)
    Ws, K = a.Ws, a.K
    h = vcat((Ws[k] * x for k = 1:K)...)
    a.h = h
    σ = Θ(vec(sum(Θ(h), dims=1)))
    return σ
end

function Base.show(io::IO, l::Comm)
    print(io, "Comm($(l.Nk), $(l.K))")
end


mutable struct LAL
    η::Float64
end


function chain(c::Comm; β = 20.0)
    Ws, Nk, K = c.Ws, c.Nk, c.K
    return Chain(
        Fullβ(Nk, K; β = β, initW = (_,k)->copy(Ws[k])),
        x->vec(sum(x, dims=1) ./ √K)
       )
end

end # module

