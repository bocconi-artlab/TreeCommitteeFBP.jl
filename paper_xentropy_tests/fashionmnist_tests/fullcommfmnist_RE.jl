module FullCommSGD_FMNIST_RE

using Flux
using Flux: data
import Flux: params
using Statistics
using DelimitedFiles
using Base.Iterators: partition, repeated, flatten
using ExtractMacro
using Distributed

include("aux.jl")
include("fullcommfmnist.jl")
using .FullCommSGD_FMNIST.DBeta

import .FullCommSGD_FMNIST: renorm!

const loss_mse = FullCommSGD_FMNIST.loss_mse
const loss_ce = FullCommSGD_FMNIST.loss_ce
const loss_lal = FullCommSGD_FMNIST.loss_lal

function runtests(conffile::String, ns; force = false)
    pmap(ns) do n
        if length(n) == 2
            nn, seed = n
            rseed = 0
        else
            nn, rseed, seed = n
        end
        @time converged, s_trnacc, s_tstacc, setup = runtest(conffile, nn; force = force, seed = seed, rseed = rseed)
    end
    allstats(conffile, ns)
end

allstats(conffile, ns; kw...) = FullCommSGD_FMNIST.allstats(conffile, ns; statsdir = statsdir, kw...)
statshess(conffile; kw...) = FullCommSGD_FMNIST.statshess(conffile; statsdir = statsdir, kw...)

function statsdir(conffile::String)
    id = @getsettings conffile begin
        M,              Int
        classes,        Tuple{Int,Int},         (2, 4)
        lim,            Tuple{Float64,Float64}, (0.25, 0.75)
        K,              Int
        B,              Int
        R,              Int
        stop_at_sol,    Bool
        algo,           Symbol

        γ,              Float64
        η,              Float64
        β,              Float64
        β₁,             Float64
        γ₁,             Float64
        ϕ,              Float64
        ϕ₁,             Float64

        epochs,         Int
        loss_threshold, Float64

        randomization,  Symbol, :none, nodefhash
    end

    @assert M % B == 0
    @assert algo ∈ [:REce, :REmse, :RElal]

    rtag = randomization == :none ? "" : ".rndz_$randomization"
    dir = "stats.cl_$(classes[1])-$(classes[2]).K_$K.M_$M" * rtag
    file_templ = "algo_$algo.stop_$stop_at_sol.id_$id"
    return id, dir, file_templ
end

mutable struct Setup{NN,OT}
    X
    Y
    X_t
    Y_t
    B::Integer
    m::NN
    loss1::Function
    opt::OT
    γ::Real
    β₁::Real
    γ₁::Real
    ϕ::Real
    ϕ₁::Real
    eps1::Int
    dir::String
    outfile::String
    epochs::Int
    stop_at_sol::Bool
    loss_threshold::Real
    id::String
end

mutable struct RepDBeta
    Nk::Int
    K::Int
    R::Int
    mc::Chain
    mr::Vector{Chain}
    β::Float64
    function RepDBeta(Nk, K, R, β)
        mr = [Chain(
                    Fullβ(Nk, K, β = β),
                    x->vec(sum(x, dims=1) ./ √K)
                   ) for i = 1:R]
        mc = Chain(
                   Fullβ(Nk, K, β = β, initW = (_,k)->mean(mr[a].layers[1].Ws[k].data for a = 1:R)),
                   # Fullβ(Nk, K, β = β),
                   x->vec(sum(x, dims=1) ./ √K)
                  )
        return new(Nk, K, R, mc, mr, β)
    end
end

params(m::RepDBeta) = params(m.mc, m.mr...)

function lossR(m::RepDBeta, x, y, loss1, γ, ϕ)
    @extract m : K R mr mc
    s = sum(loss1(m1, x1, y1, γ) for (m1, x1, y1) in zip(mr, x, y))
    Wsc = mc.layers[1].Ws
    for a = 1:R
        Wsa = mr[a].layers[1].Ws
        for k = 1:K
            s += ϕ * sum((Wsa[k] .- Wsc[k]) .^ 2)
        end
    end
    return s
end

function diststats(m::RepDBeta)
    @extract m : K R mc mr
    Wsc = mc.layers[1].Ws

    dists = Array{Float64}(undef, R)
    dk = Array{Float64}(undef, K)
    for a = 1:R
        Wsa = mr[a].layers[1].Ws
        dk .= 0
        for k = 1:K
            Wc = Wsc[k].data
            Wa = Wsa[k].data
            dk[k] = √sum((Wa .- Wc) .^ 2)
        end
        dists[a] = mean(dk)
    end
    return (mean(dists), extrema(dists)...)
end

function callback(ep, m::RepDBeta, X, Y, X_t, Y_t, γ; stop_at_sol = false)
    trnacc = data(accuracy(m, X, Y))
    s_trnacc = data(saccuracy(m, X, Y))
    tstacc = data(accuracy(m, X_t, Y_t))
    s_tstacc = data(saccuracy(m, X_t, Y_t))

    renorm!(m)

    stopflag = stop_at_sol && s_trnacc == 1
    return stopflag, (trnacc, s_trnacc, tstacc, s_tstacc, diststats(m)...)
end

accuracy(m::RepDBeta, x, y) = FullCommSGD_FMNIST.accuracy(m.mc, x, y)
saccuracy(m::RepDBeta, x, y) = FullCommSGD_FMNIST.saccuracy(m.mc, x, y)

function renorm!(m::RepDBeta)
    for m1 in (m.mc, m.mr...)
        for Wt in m1.layers[1].Ws
            @assert all(iszero, Wt.grad)
            W = Wt.data
            W ./= .√sum(W.^2, dims=2)
        end
    end
    return m
end

function updateβ!(m::RepDBeta, β::Real)
    m.β = β
    for m1 in (m.mc, m.mr...)
        m1.layers[1].β = β
    end
end

mutable struct RepComm
    Nk::Int
    K::Int
    R::Int
    mc::Comm
    mr::Vector{Comm}
    function RepComm(Nk, K, R)
        mr = [Comm(Nk, K) for i = 1:R]
        mc = Comm(Nk, K, initW = (_,k)->mean(mr[a].Ws[k] for a = 1:R))
        return new(Nk, K, R, mc, mr)
    end
end

function diststats(m::RepComm)
    @extract m : K R mc mr
    Wsc = mc.Ws

    dists = Array{Float64}(undef, R)
    dk = Array{Float64}(undef, K)
    for a = 1:R
        Wsa = mr[a].Ws
        dk .= 0
        for k = 1:K
            Wc = Wsc[k]
            Wa = Wsa[k]
            dk[k] = √sum((Wa .- Wc) .^ 2)
        end
        dists[a] = mean(dk)
    end
    return (mean(dists), extrema(dists)...)
end

accuracy(m::RepComm, x, y) = FullCommSGD_FMNIST.accuracy(m.mc, x, y)

function renorm!(m::RepComm)
    for m1 in (m.mc, m.mr...)
        for W in m1.Ws
            W ./= .√sum(W.^2, dims=2)
        end
    end
    return m
end



function runtest(conffile::String, n::Int; force = false, seed = 0, rseed = 0)

    id = @getsettings conffile begin
        M,              Int
        classes,        Tuple{Int,Int},         (2, 4)
        lim,            Tuple{Float64,Float64}, (0.25, 0.75)
        K,              Int
        B,              Int
        R,              Int
        stop_at_sol,    Bool
        algo,           Symbol

        γ,              Float64
        η,              Float64
        β,              Float64
        β₁,             Float64
        γ₁,             Float64
        ϕ,              Float64
        ϕ₁,             Float64

        epochs,         Int
        loss_threshold, Float64

        randomization,  Symbol, :none, nodefhash
    end

    @info "ID = $id"

    loss1 = algo == :REce  ? loss_ce  :
            algo == :REmse ? loss_mse :
            algo == :RElal ? loss_lal :
            error("unknown algorithm $algo")

    if algo == :RElal
        @assert γ == 0
        @assert β == 0
        @assert γ₁ == 0
        @assert β₁ == 0
    end

    @assert M % B == 0

    rtag = randomization == :none ? "" : ".rndz_$randomization"
    rtag2 = randomization == :none ? "" : ".rseed_$rseed"
    dir = joinpath("tests.cl_$(classes[1])-$(classes[2]).K_$K.M_$M" * rtag, "algo_$algo.stop_$stop_at_sol.n_$n" * rtag2 * ".seed_$seed.id_$id")
    outcf = joinpath(dir, "conf.jl")
    outfile = joinpath(dir, "log.txt")
    if isfile(outcf)
        if read(outcf) ≠ read(conffile)
            error("possible id clash, bailing out: dir=$dir")
        end
    end
    if isfile(outfile)
        force || error("outfile exists, use force=true to overwrite ($outfile)")
    else
        mkpath(dir)
    end
    cp(conffile, outcf, force = true)

    X, Y, X_t, Y_t = FullCommSGD_FMNIST.getdata(classes, M; seed = n, lim = lim, randomization = randomization, rseed = rseed)
    Xfile = joinpath(dir, "X.txt")
    writedlm(Xfile, X')
    yfile = joinpath(dir, "Y.txt")
    writedlm(yfile, Y)
    Xtfile = joinpath(dir, "X_t.txt")
    writedlm(Xtfile, X_t')
    ytfile = joinpath(dir, "Y_t.txt")
    writedlm(ytfile, Y_t)

    Nk = size(X, 1)

    Random.seed!(6480000951 + seed * 10^5)
    if algo == :RElal
        m = RepComm(Nk, K, R)
        opt = LAL(η)
    else
        m = RepDBeta(Nk, K, R, β)
        opt = Descent(η)
    end

    @show accuracy(m, X, Y)
    @show accuracy(m, X_t, Y_t)

    eps1 = 1

    open(outfile, "w") do f
        println(f, "# η=$η β₀=$β γ₀=$γ ϕ₀=$ϕ β₁=$β₁ γ₁=$γ₁ ϕ₁=$ϕ₁")
    end

    setup = Setup(X, Y, X_t, Y_t, B, m, loss1, opt,
                  γ, β₁, γ₁, ϕ, ϕ₁, eps1, dir, outfile, epochs,
                  stop_at_sol, loss_threshold, id)

    return run!(setup)
end

function run!(setup::Setup{RepDBeta}; st0 = 0)
    @extract setup : X Y X_t Y_t B m loss1 opt γ β₁ γ₁ ϕ ϕ₁ eps1 dir outfile epochs stop_at_sol loss_threshold
    @extract m : R

    converged = false
    solved = false
    for st = (1:epochs) .+ st0
        @extract setup : γ ϕ
        @extract m : β
        @info "γ=$γ β=$β ϕ=$ϕ"
        renorm!(m)
        # recenter!(m)

        M = length(Y)
        ps = [randperm(M) for a = 1:R]
        r(b) = (1:B) .+ (b-1)*B
        indata = [(X[:, ps[a][r(b)]] for a = 1:R) for b = 1:(M÷B)]
        labels = [(Y[ps[a][r(b)]] for a = 1:R) for b = 1:(M÷B)]

        result = etrain!((eps1,1),
                         (x,y)->lossR(m, x, y, loss1, γ, ϕ),
                         params(m), (indata, labels),
                         opt,
                         ep0 = (st-1) * eps1,
                         tstcallback = ep->callback(ep, m, X, Y, X_t, Y_t, γ, stop_at_sol=stop_at_sol),
                         outf = outfile,
                         loss_threshold = loss_threshold)
        β *= β₁
        updateβ!(m, β)
        setup.γ *= γ₁
        setup.ϕ *= ϕ₁
        if result == :converged
            @info "CONVERGED"
            converged = true
            break
        elseif result == :stopped
            break
        end
    end
    _, (trnacc, s_trnacc, tstacc, s_tstacc) = callback(0, m, X, Y, X_t, Y_t, γ)
    solved = s_trnacc == 1
    @info "acc = $trnacc s_acc = $s_trnacc"
    solved && @info "SOLVED"

    FullCommSGD_FMNIST.dumpweights(m.mc, dir)

    return converged, s_trnacc, s_tstacc, setup
end

function run!(setup::Setup{RepComm,LAL}; st0 = 0)
    @extract setup : X Y X_t Y_t B m loss1 opt γ β₁ γ₁ ϕ ϕ₁ eps1 dir outfile epochs stop_at_sol loss_threshold
    @extract m : R

    @assert loss1 == loss_lal
    @assert eps1 == 1

    @extract m : Nk K
    rr(k) = (1:Nk) .+ (k-1)*Nk

    converged = false
    solved = false
    for st = (1:epochs) .+ st0
        @extract setup : ϕ
        @extract opt : η
        @info "ϕ=$ϕ, η=$η"
        renorm!(m)

        M = length(Y)
        ps = [randperm(M) for a = 1:R]
        r(b) = (1:B) .+ (b-1)*B
        indata = [(X[:, ps[a][r(b)]] for a = 1:R) for b = 1:(M÷B)]
        labels = [(Y[ps[a][r(b)]] for a = 1:R) for b = 1:(M÷B)]

        l = 0.0
        for (xb, yb) in zip(indata, labels)
            for (a,x,y) in zip(1:R, xb, yb)
                ma = m.mr[a]
                mc = m.mc
                Ws = ma.Ws
                @assert length(y) == B
                σ = ma(x)
                h = ma.h
                for b = 1:B
                    σ[b] * y[b] > 0 && continue

                    kₘ = 0
                    δₘ = eltype(h)(-Inf)
                    for k = 1:K
                        δ = h[k,b] * y[b]
                        δ > 0 && continue
                        if δ > δₘ
                            kₘ = k
                            δₘ = δ
                        end
                    end
                    l -= δₘ
                    Ws[kₘ] .+= sum((η * y[b]) .* @view(x[:,b]), dims=2)'
                end
                for k in 1:K
                    d = (mc.Ws[k] - Ws[k])
                    Ws[k] .+= η .* ϕ .* d
                    mc.Ws[k] .-= η .* ϕ .* d
                    l += ϕ * sum(d.^2)
                end
            end
        end
        acc = accuracy(m, X, Y)
        tstacc = accuracy(m, X_t, Y_t)
        outstr = "  epoch $st: loss=$l ($acc $tstacc) $(diststats(m))"
        println(outstr)
        outfile ≠ nothing && open(f->println(f, outstr), outfile, "a")

        setup.ϕ *= ϕ₁
        # setup.η *= η₁
        if l ≤ loss_threshold
            @info "CONVERGED"
            converged = true
            break
        elseif stop_at_sol && acc == 1
            break
        end
    end
    acc = accuracy(m, X, Y)
    tstacc = accuracy(m, X_t, Y_t)
    solved = acc == 1
    @info "acc = $acc tstacc = $tstacc"
    solved && @info "SOLVED"

    renorm!(m)
    FullCommSGD_FMNIST.dumpweights(m.mc, dir)

    return converged, acc, tstacc, setup
end

end # module FullCommSGD_FMNIST_RE

