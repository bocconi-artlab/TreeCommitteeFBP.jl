using Random

ce(x, γ) = -x/2 + 1/2γ * log(2*cosh(γ*x))                   # binary ce loss which tends to ReLU as γ → ∞
ce_ren(x, γ) = -γ * x/log(2) + log(2*cosh(γ*x)) / log(2)    # same, but diverges for γ → ∞  and x < 0; scaled such that it's always 1 in x=0

exprange(start, stop, steps) = exp.(LinRange(log(start), log(stop), steps))

dprint(d) = join(("$i=>$(d[i])" for i in sort!(collect(keys(d)))), ", ")

bad_id(x) = lpad(string(Int(hash(x) >>> 46)), 6, '0')

function randpatt(N, M, n)
    Random.seed!(3330000888 + n * 10^5)
    return 2.0 * bitrand(N, M) .- 1
end

macro getsettings(conffile, specs)
    ex = quote
        lines = map(x->split(x, r"\s*=\s*"), filter(!isempty, readlines($(esc(conffile)))))
        d = Dict(k => v for (k,v) in lines)
        h = []
    end
    @assert Meta.isexpr(specs, :block)
    for spec in specs.args
        spec isa LineNumberNode && continue
        Meta.isexpr(spec, :tuple) || (@warn "skipping spec $spec"; continue)
        n, T = spec.args[1:2]
        def = length(spec.args) > 2 ? spec.args[3] : nothing
        dohash = :always
        if length(spec.args) > 3
            if spec.args[4] == :nohash
                dohash = :never
            elseif spec.args[4] == :nodefhash
                dohash = :ifmissing
            else
                @warn "ignoring unknown directive $(spec.args[4])"
            end
        end
        s = String(n)
        ex = quote
            $ex
            dohash = $(dohash ≠ :never)
            if haskey(d, $s)
                v = d[$s]
                $(esc(n)) = $T(eval(Meta.parse(v)))
            else
                $(esc(n)) = $(esc(def))
                dohash = $(dohash == :always)
            end
            dohash && push!(h, ($s, $(esc(n))))
        end
    end
    ex = quote
        $ex
        bad_id(sort!(h, by=first))
    end
    return ex
end


macro opt(kw, n, default = nothing)
    s = Expr(:call, :Symbol, String(n))
    n = esc(n)
    kw = esc(kw)
    default = esc(default)
    ex = quote
        $n = $default
        if haskey($kw, $s)
            $n = $kw[$s]
            delete!($kw, $s)
        end
    end
    return ex
end

rzip(a, b, n) = flatten(repeated(zip(a, b), n))
function etrain!(epspec, loss, params, data, opt; kw...)
    epochs, epgap = epspec
    kw = Dict(kw...)
    @opt kw tstcallback
    @opt kw outf
    @opt kw loss_threshold NaN
    @opt kw ep0 0
    for ep in 1:(epochs÷epgap)
        Flux.train!(loss, params, rzip(data..., epgap), opt; kw...)
        l = mean(loss.(data...))
        outstr = "  epoch $(ep0 + ep*epgap): loss=$l"
        stopflag = false
        if tstcallback ≢ nothing
            stopflag, tstret = tstcallback(ep0 + ep*epgap)
            outstr *= " $tstret"
        end
        println(outstr)
        outf ≠ nothing && open(f->println(f, outstr), outf, "a")
        stopflag && return :stopped
        l < loss_threshold && return :converged
    end
    return :ok
end
