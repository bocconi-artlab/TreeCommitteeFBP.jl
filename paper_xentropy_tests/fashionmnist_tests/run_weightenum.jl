module RunWeightEnum

include(joinpath(@__DIR__, "../../src/TreeCommitteeFBP.jl"))
using .TreeCommitteeFBP
using Statistics
using DelimitedFiles
using ExtractMacro
using Distributed

using .TreeCommitteeFBP: ⊗, ⊘, ↑

include("aux.jl")

include("fullcommfmnist.jl")
include("fullcommfmnist_RE.jl")
const SEEDS = FullCommSGD_FMNIST.SEEDS

function dumpW(filename::AbstractString, μs::Vector{Vector{Float64}})
    writedlm(filename, vcat((w' for w in μs)...))
end
function parseW(filename::AbstractString)
    wm = readdlm(filename)
    K, Nk = size(wm)
    N = K * Nk
    w = vcat((wm[k,:] for k = 1:K)...)
    return N, K, w
end

function runtests(conffile::String, ns::Vector{SEEDS}; force = false)
    for n in ns
        @time solved, X, Y, Ws = run_fBP(conffile, n; force = force)
    end
    FullCommSGD_FMNIST.allstats(conffile, ns; statsdir = statsdir)
end

allstats(conffile, ns; kw...) = FullCommSGD_FMNIST.allstats(conffile, ns; statsdir = statsdir, kw...)
statshess(conffile; kw...) = FullCommSGD_FMNIST.statshess(conffile; statsdir = statsdir, kw...)

function statsdir(conffile::String)
    id = @getsettings conffile begin
        M,           Int
        classes,     Tuple{Int,Int},         (2, 4)
        lim,         Tuple{Float64,Float64}, (0.25, 0.75)
        K,           Int
        stop_at_sol, Bool

        max_iters,   Int,              200
        max_steps,   Int,              100
        damping,     Float64,          0.0
        accuracy1,   Symbol,           :accurate
        scoping,     Vector,           [(γ,10.0) for γ in exprange(0.5, 50.0, 30)]
        ϵ,           Float64,          1e-2
        rand_fact,   Float64,          0.0
        κ,           Int,              0

        randomization,  Symbol, :none, nodefhash
    end

    algo = :fBP

    rtag = randomization == :none ? "" : ".rndz_$randomization"
    dir = "stats.cl_$(classes[1])-$(classes[2]).K_$K.M_$M" * rtag
    file_templ = "algo_$algo.stop_$stop_at_sol.id_$id"
    return id, dir, file_templ
end

function run_fBP(conffile::String, n::Int, seed::Int; force::Bool = false, rseed::Int = 0)

    id = @getsettings conffile begin
        M,           Int
        classes,     Tuple{Int,Int},         (2, 4)
        lim,         Tuple{Float64,Float64}, (0.25, 0.75)
        K,           Int
        stop_at_sol, Bool

        max_iters,   Int,              200
        max_steps,   Int,              100
        damping,     Float64,          0.0
        accuracy1,   Symbol,           :accurate
        scoping,     Vector,           [(γ,10.0) for γ in exprange(0.5, 50.0, 30)]
        ϵ,           Float64,          1e-2
        rand_fact,   Float64,          0.0
        κ,           Int,              0

        randomization,  Symbol, :none, nodefhash
    end

    @info "ID = $id"

    fprotocol = FreeScoping(scoping)

    algo = :fBP

    rtag = randomization == :none ? "" : ".rndz_$randomization"
    rtag2 = randomization == :none ? "" : ".rseed_$rseed"
    dir = joinpath("tests.cl_$(classes[1])-$(classes[2]).K_$K.M_$M" * rtag, "algo_$algo.stop_$stop_at_sol.n_$n" * rtag2 * ".seed_$seed.id_$id")
    outcf = joinpath(dir, "conf.jl")
    outfile = joinpath(dir, "log.txt")
    if isfile(outcf)
        if read(outcf) ≠ read(conffile)
            error("possible id clash, bailing out: dir=$dir")
        end
    end
    if isfile(outfile)
        force || error("outfile exists, use force=true to overwrite ($outfile)")
    else
        mkpath(dir)
    end
    cp(conffile, outcf, force = true)

    X, Y, X_t, Y_t = FullCommSGD_FMNIST.getdata(classes, M; seed = n, lim = lim, randomization = randomization, rseed = rseed)
    Xfile = joinpath(dir, "X.txt")
    writedlm(Xfile, X')
    yfile = joinpath(dir, "Y.txt")
    writedlm(yfile, Y)
    Xtfile = joinpath(dir, "X_t.txt")
    writedlm(Xtfile, X_t')
    ytfile = joinpath(dir, "Y_t.txt")
    writedlm(ytfile, Y_t)

    Xr = repeat(X, K)
    X_BP = TreeCommitteeFBP.Patterns([Xr[:,a] for a = 1:M], Y)

    Nk = size(X, 1)
    N = Nk * K

    seed_BP = 6560000911 + seed * 10^5

    errs, messages, _, params =
        focusingBP(N, K, X_BP, max_iters=max_iters, max_steps=max_steps, seed=seed_BP,
                   damping=damping, accuracy1=accuracy1, fprotocol=fprotocol,
                   ϵ=ϵ, outatzero=stop_at_sol, writeoutfile=:always,
                   outfile=outfile, rand_fact=rand_fact)

    solved = errs == 0

    @extract messages : mw ux
    μs = [Vector{Float64}(undef, Nk) for k = 1:K]
    for k = 1:K, i = 1:Nk
        μs[k][i], _ = float(mw[k][i])
    end
    dumpW(joinpath(dir, "W.txt"), μs)

    @extract params : r γ χst
    μx = [Vector{Float64}(undef, Nk) for k = 1:K]
    y = r + 1
    for k = 1:K, i = 1:Nk
        hkix = mw[k][i] ⊘ ux[k][i]
        mx = ((hkix * γ) ↑ y) ⊗ χst
        μx[k][i], _ = float(mx)
    end
    dumpW(joinpath(dir, "Wstar.txt"), μx)

    return solved, X, Y, μs
end

function run_WEF(conffile::String, n::SEEDS, algo::Symbol;
                 max_iters::Integer = 200,
                 seed::Integer = 8723742,
                 damping::Real = 0.0,
                 accuracy1::Symbol = :accurate,
                 # γl = [0.0:1e-2:1.0..., 1.1:0.1:10.0..., 11.0:1:50...],
                 # γl = [0.0, exp.(LinRange(log(0.1), log(50), 50))...],
                 γr::Tuple{<:Real,<:Real,<:Real} = (0.1, 200.0, 50),
                 ϵ::Real = 1e-3,
                 # tag::AbstractString = "",
                 ifexists::Symbol = :skip
                )
    ifexists ∈ [:overwrite, :skip, :error] || throw(ArgumentError("ifexists must be either :overwrite, :skip or :error; given: $ifexists"))
    algo ∈ [:ce, :mse, :lal, :fBP, :REce, :REmse, :RElal] || throw(ArgumentError("unknown algorithm: $algo"))

    if algo == :fBP
        id = @getsettings conffile begin
            M,           Int
            classes,     Tuple{Int,Int},         (2, 4)
            lim,         Tuple{Float64,Float64}, (0.25, 0.75)
            K,           Int
            stop_at_sol, Bool

            max_iters,   Int,              200
            max_steps,   Int,              100
            seed,        Int,              678876
            damping,     Float64,          0.0
            accuracy1,   Symbol,           :accurate
            scoping,     Vector,           [(γ,10.0) for γ in exprange(0.5, 50.0, 30)]
            ϵ,           Float64,          1e-2
            rand_fact,   Float64,          0.0
            κ,           Int,              0

            randomization,  Symbol, :none, nodefhash
        end

        @info "ID = $id"

        N % K == 0 || throw(ArgumentError("invalid K: N=$N K=$K"))
        fprotocol = FreeScoping(scoping)
        α = round(M / N, digits=1) # NOT NICE!
    elseif algo ∈ [:REce, :REmse, :RElal]
        id = @getsettings conffile begin
            M,              Int
            classes,        Tuple{Int,Int},         (2, 4)
            lim,            Tuple{Float64,Float64}, (0.25, 0.75)
            K,              Int
            B,              Int
            R,              Int
            stop_at_sol,    Bool
            algo,           Symbol

            γ,              Float64
            η,              Float64
            β,              Float64
            β₁,             Float64
            γ₁,             Float64
            ϕ,              Float64
            ϕ₁,             Float64

            epochs,         Int
            loss_threshold, Float64

            randomization,  Symbol, :none, nodefhash
        end

        @info "ID = $id"

        if algo == :RElal
            @assert γ == 0
            @assert β == 0
            @assert γ₁ == 0
            @assert β₁ == 0
        end

        @assert M % B == 0
    else
        id = @getsettings conffile begin
            M,              Int
            classes,        Tuple{Int,Int},         (2, 4)
            lim,            Tuple{Float64,Float64}, (0.25, 0.75)
            K,              Int
            B,              Int
            stop_at_sol,    Bool
            algo,           Symbol

            γ,              Float64
            η,              Float64
            β,              Float64
            β₁,             Float64
            γ₁,             Float64

            epochs,         Int
            loss_threshold, Float64

            randomization,  Symbol, :none, nodefhash
        end

        @info "ID = $id"

        if algo == :lal
            @assert γ == 0
            @assert β == 0
            @assert γ₁ == 0
            @assert β₁ == 0
            @assert stop_at_sol
        end
    end

    if length(n) == 2
        nn, seed = n
        rseed = 0
    else
        nn, rseed, seed = n
    end
    rtag = randomization == :none ? "" : ".rndz_$randomization"
    rtag2 = randomization == :none ? "" : ".rseed_$rseed"
    dir = joinpath("tests.cl_$(classes[1])-$(classes[2]).K_$K.M_$M" * rtag, "algo_$algo.stop_$stop_at_sol.n_$nn" * rtag2 * ".seed_$seed.id_$id")
    isdir(dir) || error("directory not found: $dir")
    Xfile = joinpath(dir, "X.txt")
    X = Matrix(readdlm(Xfile)')
    Yfile = joinpath(dir, "Y.txt")
    Y = vec(readdlm(Yfile))
    Xr = repeat(X, K)
    X_BP = TreeCommitteeFBP.Patterns([Xr[:,a] for a = 1:M], Y)

    solfile = joinpath(dir, "W.txt")
    isfile(solfile) || throw(ArgumentError("file not found: $solfile"))
    N, K, wp = parseW(solfile)
    N == length(first(X_BP.X)) || throw(ArgumentError("incompatible patterns and solutions, Nsol=$N Npatt=$(length(first(X_BP.X)))"))

    γl = [0.0, exp.(LinRange(log(γr[1]), log(γr[2]), γr[3]))...]

    # tag ≠ "" && (tag = "$tag.")
    outfile = joinpath(dir, "wef.txt")
    if isfile(outfile)
        if ifexists == :error
            throw(ArgumentError("out file exists, use force=true to overwrite: $outfile"))
        elseif ifexists == :skip
            @info "  outfile exists, skipping"
            return
        end
        rm(outfile)
    end

    weight_enum(N, K, X_BP, wp, max_iters=max_iters, seed=seed,
                damping=damping, accuracy1=accuracy1, γl=γl,
                ϵ=ϵ, outfile=outfile)
    return
end

function run_all_WEFs(conffile::String, ns, algo::Symbol; kw...)
    pmap(ns) do n
        @info "n = $n"
        run_WEF(conffile, n, algo; kw...)
    end
end

function average_WEF(conffile::String, algo::Symbol; onlysolved = true)
    algo ∈ [:ce, :mse, :lal, :fBP, :REce, :REmse, :RElal] || throw(ArgumentError("unknown algorithm: $algo"))

    if algo == :fBP
        id, dir, file_templ = statsdir(conffile)
    elseif algo ∈ [:REce, :REmse, :RElal]
        id, dir, file_templ = FullCommSGD_FMNIST_RE.statsdir(conffile)
    else
        id, dir, file_templ = FullCommSGD_FMNIST.statsdir(conffile)
    end
    statsfile = joinpath(dir, "stats.$file_templ.txt")
    isfile(statsfile) || error("stats file not found: $statsfile")
    tst_basedir = replace(dir, "stats" => "tests")
    isdir(tst_basedir) || error("tests dir not found: $tst_basedir")

    ns, unsolved = open(statsfile, "r") do f
        l = readline(f)
        startswith(l, "ns = ") || error("invalid ns line: $l")
        ns = eval(Meta.parse(replace(l, "ns = "=>"")))::AbstractVector
        l = readline(f)
        startswith(l, "unsolved = ") || error("invlide unsolved line: $l")
        unsolved = eval(Meta.parse(replace(l, "unsolved = " => "")))::Vector{<:SEEDS}
        return ns, unsolved
    end

    stats_d = Dict{Float64,Vector{Float64}}()
    stats_S = Dict{Float64,Vector{Float64}}()
    outfile = joinpath(dir, "wef_averages.$file_templ.txt")

    for n in ns
        onlysolved && ns ∈ unsolved && continue
        if length(n) == 2
            tst_dir1 = replace(file_templ, ".id_" => ".n_$(n[1]).seed_$(n[2]).id_") # XXX horrible hack!
        else
            tst_dir1 = replace(file_templ, ".id_" => ".n_$(n[1]).rseed_$(n[2]).seed_$(n[3]).id_") # XXX horrible hack!
        end
        tst_dir = joinpath(tst_basedir, tst_dir1)
        infile = joinpath(tst_dir, "wef.txt")
        isfile(infile) || (@info "  SKIPPING (FILE NOT FOUND: $infile)"; continue)
        @info "file $infile"
        open(infile) do f
            l = readline(f)
            @assert startswith(l, "#")
            for l in readlines(f)
                ls = split(l)
                converged = parse(Bool, ls[1])
                converged || continue
                γ, S, d = map(x->parse(Float64, x), ls[[2,5,8]])
                vd = get!(stats_d, γ, Float64[])
                vS = get!(stats_S, γ, Float64[])
                push!(vd, d)
                push!(vS, S)
            end
        end
    end
    outd = Dict(γ=>(mean(stats_d[γ]), std(stats_d[γ])/√length(stats_d[γ]), mean(stats_S[γ]), std(stats_S[γ])/√length(stats_S[γ])) for γ in keys(stats_d)) # if length(stats_d[γ])>4)
    open(outfile, "w") do f
        println(f, "#γ ⟨d⟩ (std) ⟨Σᵢₙₜ⟩ (std)")
        for γ in sort!(collect(keys(outd)))
            println(f, "$γ ", join(map(string, outd[γ]), " "))
        end
    end
end

end # module
