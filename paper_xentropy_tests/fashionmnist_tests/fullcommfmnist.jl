module FullCommSGD_FMNIST

using Flux
using Flux: data
using Base.Iterators: partition, repeated, flatten
using Random, Statistics, LinearAlgebra, DelimitedFiles
using ExtractMacro
using Distributed

include("aux.jl")
include("DBeta.jl")
using .DBeta
include("hess.jl")

function getdata(classes, M; seed = 0, lim = (0.25, 0.75), randomization = :none, rseed = 0)
    @assert length(classes) == 2
    @assert iseven(M)
    @assert randomization ∈ [:none, :inputs, :all]
    imgs = [vec(float.(img)) for img in Flux.Data.FashionMNIST.images()]
    lbls = Flux.Data.FashionMNIST.labels()

    imgs_t = [vec(float.(img)) for img in Flux.Data.FashionMNIST.images(:test)]
    lbls_t = Flux.Data.FashionMNIST.labels(:test)

    N = length(imgs[1])
    X = zeros(N, M)
    Y = zeros(M)

    valid = Vector{Int}[Int[], Int[]]

    Random.seed!(1350000975 + seed * 10^5)
    # train
    for (s,c) in enumerate(classes)
        σ = 2s - 3 # convert to ±1
        cimgs = imgs[lbls .== c]
        meds = median.(cimgs)
        valid[s] = findall(lim[1] .≤ meds .≤ lim[2])
        shuffle!(valid[s])
        M0 = (M÷2) * (s - 1)
        for a = 1:(M÷2)
            i = valid[s][a]
            a0 = a + M0
            # @show sum(2 .* (cimgs[i] .≥ meds[i]) .- 1)
            X[:,a0] = 2 .* (cimgs[i] .≥ meds[i]) .- 1
            Y[a0] = σ
        end
    end

    # remainder of train, to be used as test?
    valid_r = Vector{Int}[Int[], Int[]]
    for s = 1:2
        valid_r[s] = valid[s][(M÷2)+1:end]
    end
    M_r = length.(valid_r)
    X_r = zeros(N, sum(M_r))
    Y_r = zeros(sum(M_r))
    for (s,c) in enumerate(classes)
        σ = 2s - 3 # convert to ±1
        cimgs = imgs[lbls .== c]
        meds = median.(cimgs)
        M0 = s == 1 ? 0 : M_r[1]
        for a = 1:M_r[s]
            i = valid_r[s][a]
            a0 = a + M0
            X_r[:,a0] = 2 .* (cimgs[i] .≥ meds[i]) .- 1
            Y_r[a0] = σ
        end
    end

    # test
    valid_t = Vector{Int}[Int[], Int[]]
    for (s,c) in enumerate(classes)
        cimgs_t = imgs_t[lbls_t .== c]
        meds_t = median.(cimgs_t)
        valid_t[s] = findall(lim[1] .≤ meds_t .≤ lim[2])
    end
    M_t = length.(valid_t)
    X_t = zeros(N, sum(M_t))
    Y_t = zeros(sum(M_t))
    for (s,c) in enumerate(classes)
        σ = 2s - 3 # convert to ±1
        cimgs_t = imgs_t[lbls_t .== c]
        meds_t = median.(cimgs_t)
        M0 = s == 1 ? 0 : M_t[1]
        for a = 1:M_t[s]
            i = valid_t[s][a]
            a0 = a + M0
            X_t[:,a0] = 2 .* (cimgs_t[i] .≥ meds_t[i]) .- 1
            Y_t[a0] = σ
        end
    end

    # X_t = hcat(X_t, X_r)
    # Y_t = vcat(Y_t, Y_r)
    #
    # # M1 = min(sum(Y_t .== 1), sum(Y_t .== -1))
    # # rinds = vcat(findall(Y_t .== -1)[1:M1], findall(Y_t .== 1)[1:M1])
    # # X_t = X_t[:,rinds]
    # # Y_t = Y_t[rinds]
    #
    # n_t = [sum(Y_t .== σ) for σ = [-1,1]]
    # smin = argmin(n_t)
    # smax = smin == 1 ? 2 : 1
    # inds_add = vcat(1:n_t[smin], rand(1:n_t[smin], n_t[smax] - n_t[smin]))
    # if smin == 1
    #     rinds = vcat(findall(Y_t .== -1)[inds_add], findall(Y_t .== 1))
    # else
    #     rinds = vcat(findall(Y_t .== -1), findall(Y_t .== 1)[inds_add])
    # end
    # @assert length(rinds) == 2n_t[smax]
    # X_t = X_t[:,rinds]
    # Y_t = Y_t[rinds]



    if randomization ≠ :none
        Random.seed!(8110000522 + rseed * 10^5)
    end
    if randomization == :inputs
        for σ = [-1,1], i = 1:N
            shuffle!(@view(X[i,Y .== σ]))
            # shuffle!(@view(X_t[i,Y_t .== σ]))
        end
    elseif randomization == :all
        for i = 1:N
            shuffle!(@view(X[i,:]))
            # shuffle!(Y)
            # shuffle!(@view(X_t[i,:]))
        end
    end
    return X, Y, X_t, Y_t
end

function renorm!(ms::Chain...)
    for m in ms, Wt in m.layers[1].Ws
        @assert all(iszero, Wt.grad)
        W = Wt.data
        W ./= .√sum(W.^2, dims=2)
    end
end
function renorm!(m::Comm)
    for W in m.Ws
        W ./= .√sum(W.^2, dims=2)
    end
end

function loss_mse(m, x, y, γ)
    return sum((tanh.(m(x) .* γ) .- y).^2)
end

function loss_ce(m, x, y, γ)
    return sum(map(z->ce(z, γ), m(x) .* y))
end

function loss_lal(m::Comm, x, y)
    ŷ = m(x)
    @extract m : Ws K h
    s = zero(eltype(h))
    for a = 1:length(ŷ)
        ŷ[a] * y[a] > 0 && continue

        δₘ = eltype(h)(-Inf)
        for k = 1:K
            δ = h[k,a] * y[a]
            δ > 0 && continue
            δₘ = max(δₘ, δ)
        end
        s -= δₘ
    end
    return s
end

accuracy(m, x, y) = mean(sign.(m(x)) .== y)
function saccuracy(m, x, y)
    oldβ = m.layers[1].β
    m.layers[1].β = floatmax(oldβ)
    acc = accuracy(m, x, y)
    m.layers[1].β = oldβ
    return acc
end

function callback(ep, m, X, Y, X_t, Y_t, γ; stop_at_sol = false)
    trnacc = data(accuracy(m, X, Y))
    s_trnacc = data(saccuracy(m, X, Y))
    tstacc = data(accuracy(m, X_t, Y_t))
    s_tstacc = data(saccuracy(m, X_t, Y_t))

    renorm!(m)

    stopflag = stop_at_sol && s_trnacc == 1
    return stopflag, (trnacc, s_trnacc, tstacc, s_tstacc)
end

mutable struct Setup{NN,OT}
    X
    Y
    X_t
    Y_t
    B
    m::NN
    loss::Function
    opt::OT
    γ::Real
    β₁::Real
    γ₁::Real
    eps1::Int
    dir::String
    outfile::String
    epochs::Int
    stop_at_sol::Bool
    loss_threshold::Real
    id::String
end

const SEEDS = Union{NTuple{2,Int},NTuple{3,Int}}

function runtests(conffile::String, ns::Vector{<:SEEDS}; force = false)

    unsolved = SEEDS[]
    β0s = Dict{SEEDS,Float64}()
    accs = Dict{SEEDS,Float64}()
    tstaccs = Dict{SEEDS,Float64}()
    id, dir, file_templ = statsdir(conffile)
    mkpath(dir)

    # for n in ns
    results = pmap(ns) do n
        if length(n) == 2
            nn, seed = n
            rseed = 0
        else
            nn, rseed, seed = n
        end
        @time converged, acc, tstacc, setup = runtest(conffile, nn; force = force, seed=seed, rseed=rseed)
        solved = acc == 1
        @assert id == setup.id
        if !solved
            push!(unsolved, n)
        end
        β0 = round(seek_minβ(setup, tol=1e-2), RoundUp)
        # β0s[n] = β0
        # accs[n] = acc
        # tstaccs[n] = tstacc
        return n, β0, acc, tstacc
    end
    for (n, β0, acc, tstacc) in results
        β0s[n] = β0
        accs[n] = acc
        tstaccs[n] = tstacc
    end
    statsfile = joinpath(dir, "stats.$file_templ.txt")
    open(statsfile, "w") do f
        print(f,
              """
              ns = $ns
              unsolved = $unsolved
              solved_frac = $(1 - length(unsolved) / length(ns))
              β0s = $(dprint(β0s))
              accuracies = $(dprint(accs))
              tst_accuracies = $(dprint(tstaccs))
              """)
    end
end

function allstats(conffile::String, ns::Vector{<:SEEDS}; statsdir = statsdir)
    id, dir, file_templ = statsdir(conffile)
    tst_basedir = replace(dir, "stats" => "tests")
    isdir(tst_basedir) || error("tests dir not found: $tst_basedir")

    unsolved = SEEDS[]
    β0s = Dict{SEEDS,Float64}()
    accs = Dict{SEEDS,Float64}()
    tstaccs = Dict{SEEDS,Float64}()

    for n in ns
        if length(n) == 2
            tst_dir1 = replace(file_templ, ".id_" => ".n_$(n[1]).seed_$(n[2]).id_") # XXX horrible hack!
        else
            tst_dir1 = replace(file_templ, ".id_" => ".n_$(n[1]).rseed_$(n[2]).seed_$(n[3]).id_") # XXX horrible hack!
        end
        tst_dir = joinpath(tst_basedir, tst_dir1)

        Xfile = joinpath(tst_dir, "X.txt")
        X = Matrix(readdlm(Xfile)')
        # Nk, M = size(X)
        Yfile = joinpath(tst_dir, "Y.txt")
        Y = vec(readdlm(Yfile))
        Xtfile = joinpath(tst_dir, "X_t.txt")
        X_t = Matrix(readdlm(Xtfile)')
        Ytfile = joinpath(tst_dir, "Y_t.txt")
        Y_t = vec(readdlm(Ytfile))

        Wmat = readdlm(joinpath(tst_dir, "W.txt"))
        Ws = [Wmat[k:k,:] for k = 1:size(Wmat,1)]

        m = Comm(Ws)
        acc = accuracy(m, X, Y)
        tstacc = accuracy(m, X_t, Y_t)
        @info "acc = $acc tstacc = $tstacc"

        solved = acc == 1
        if !solved
            push!(unsolved, n)
        end
        β0 = round(seek_minβ(X, Y, m, tol=1e-2), RoundUp)
        β0s[n] = β0
        accs[n] = acc
        tstaccs[n] = tstacc
    end
    mkpath(dir)
    statsfile = joinpath(dir, "stats.$file_templ.txt")
    open(statsfile, "w") do f
        print(f,
              """
              ns = $ns
              unsolved = $unsolved
              solved_frac = $(1 - length(unsolved) / length(ns))
              β0s = $(dprint(β0s))
              accuracies = $(dprint(accs))
              tst_accuracies = $(dprint(tstaccs))
              """)
    end
end

function statshess(conffile; βs = [1.0:5.0; 10.0:5.0:50.0], onlysolved = true, statsdir = statsdir)
    id, dir, file_templ = statsdir(conffile)
    statsfile = joinpath(dir, "stats.$file_templ.txt")
    isfile(statsfile) || error("stats file not found: $statsfile")
    tst_basedir = replace(dir, "stats" => "tests")
    isdir(tst_basedir) || error("tests dir not found: $tst_basedir")

    allevs = Dict{Float64,Vector{Float64}}()
    maxevs = Dict{Float64,Dict{SEEDS,Float64}}()

    ns, unsolved, β0s = open(statsfile, "r") do f
        l = readline(f)
        startswith(l, "ns = ") || error("invalid ns line: $l")
        ns = eval(Meta.parse(replace(l, "ns = "=>"")))::AbstractVector
        l = readline(f)
        startswith(l, "unsolved = ") || error("invlid unsolved line: $l")
        unsolved = eval(Meta.parse(replace(l, "unsolved = " => "")))::Vector{SEEDS}
        l = readline(f)
        startswith(l, "solved_frac = ") || error("unexpected line: $l")
        l = readline(f)
        startswith(l, "β0s = ") || error("invalid β0s line: $l")
        β0s = eval(Meta.parse("Dict{$SEEDS,Float64}(" * replace(l, "β0s = " => "") * ")"))::Dict{SEEDS,Float64}
        return ns, unsolved, β0s
    end

    # for n in ns
    p_evs = pmap(ns) do n
        allevs = Dict{Float64,Vector{Float64}}()
        maxevs = Dict{Float64,Dict{SEEDS,Float64}}()
        onlysolved && ns ∈ unsolved && return allevs, maxevs
        if length(n) == 2
            tst_dir = joinpath(tst_basedir, replace(file_templ, ".id" => ".n_$(n[1]).seed_$(n[2]).id")) # XXX horrible hack
        else
            tst_dir = joinpath(tst_basedir, replace(file_templ, ".id" => ".n_$(n[1]).rseed_$(n[2]).seed_$(n[3]).id")) # XXX horrible hack
        end
        pattfile = joinpath(tst_dir, "X.txt")
        X = Matrix(readdlm(pattfile)')
        Nk, M = size(X)
        Yfile = joinpath(tst_dir, "Y.txt")
        Y = vec(readdlm(Yfile))
        Wmat = readdlm(joinpath(tst_dir, "W.txt"))
        Ws = [Wmat[k:k,:] for k = 1:size(Wmat,1)]

        β0 = β0s[n]
        for β in βs
            β ≥ β0 || continue
            @info "computing Hessian for n = $n β = $β"
            @time H, ev = comp_hessian(X, Y, Ws, tst_dir, β = β, correction=false);
            maxev = maximum(ev)
            @info "  n=$n β=$β : max ev = $maxev"
            haskey(allevs, β) || (allevs[β] = Float64[])
            haskey(maxevs, β) || (maxevs[β] = Dict{SEEDS,Float64}())
            append!(allevs[β], ev)
            maxevs[β][n] = maxev
        end
        return allevs, maxevs
    end

    for (pall, pmax) in p_evs
        for (β, evs) in pall
            haskey(allevs, β) || (allevs[β] = Float64[])
            append!(allevs[β], evs)
        end
        for (β, devs) in pmax
            haskey(maxevs, β) || (maxevs[β] = Dict{SEEDS,Float64}())
            for (n,maxev) in devs
                maxevs[β][n] = maxev
            end
        end
    end

    for (β,ev) in allevs
        sort!(ev, rev=true)
        evfilen = joinpath(dir, "all_eigvals.$file_templ.beta_$β.gamma_1.0.corr_false.os_$onlysolved.txt")
        writedlm(evfilen, ev)
    end
    for (β,ev) in maxevs
        evfilen = joinpath(dir, "max_eigvals.$file_templ.beta_$β.gamma_1.0.corr_false.os_$onlysolved.txt")
        open(evfilen, "w") do f
            println(f, dprint(ev))
        end
    end
end




function statsdir(conffile::String)
    id = @getsettings conffile begin
        M,              Int
        classes,        Tuple{Int,Int},         (2, 4)
        lim,            Tuple{Float64,Float64}, (0.25, 0.75)
        K,              Int
        B,              Int
        stop_at_sol,    Bool
        algo,           Symbol

        γ,              Float64
        η,              Float64
        β,              Float64
        β₁,             Float64
        γ₁,             Float64

        epochs,         Int
        loss_threshold, Float64

        randomization,  Symbol, :none, nodefhash
    end

    if algo == :lal
        @assert γ == 0
        @assert β == 0
        @assert γ₁ == 0
        @assert β₁ == 0
        @assert stop_at_sol
    end
    @assert M % B == 0

    rtag = randomization == :none ? "" : ".rndz_$randomization"
    dir = "stats.cl_$(classes[1])-$(classes[2]).K_$K.M_$M" * rtag
    file_templ = "algo_$algo.stop_$stop_at_sol.id_$id"
    return id, dir, file_templ
end


function runtest(conffile::String, n::Int; force = false, seed = 0, rseed = 0)

    id = @getsettings conffile begin
        M,              Int
        classes,        Tuple{Int,Int},         (2, 4)
        lim,            Tuple{Float64,Float64}, (0.25, 0.75)
        K,              Int
        B,              Int
        stop_at_sol,    Bool
        algo,           Symbol

        γ,              Float64
        η,              Float64
        β,              Float64
        β₁,             Float64
        γ₁,             Float64

        epochs,         Int
        loss_threshold, Float64

        randomization,  Symbol, :none, nodefhash
    end

    @info "ID = $id"

    loss = algo == :ce  ? loss_ce  :
           algo == :mse ? loss_mse :
           algo == :lal ? loss_lal :
           error("unknown algorithm $algo")

    if algo == :lal
        @assert γ == 0
        @assert β == 0
        @assert γ₁ == 0
        @assert β₁ == 0
        @assert stop_at_sol
    end

    @assert M % B == 0

    rtag = randomization == :none ? "" : ".rndz_$randomization"
    rtag2 = randomization == :none ? "" : ".rseed_$rseed"
    dir = joinpath("tests.cl_$(classes[1])-$(classes[2]).K_$K.M_$M" * rtag, "algo_$algo.stop_$stop_at_sol.n_$n" * rtag2 * ".seed_$seed.id_$id")
    outcf = joinpath(dir, "conf.jl")
    outfile = joinpath(dir, "log.txt")
    if isfile(outcf)
        if read(outcf) ≠ read(conffile)
            error("possible id clash, bailing out: dir=$dir")
        end
    end
    if isfile(outfile)
        force || error("outfile exists, use force=true to overwrite ($outfile)")
    else
        mkpath(dir)
    end
    cp(conffile, outcf, force = true)

    X, Y, X_t, Y_t = getdata(classes, M; seed = n, lim = lim, randomization = randomization, rseed = rseed)
    Xfile = joinpath(dir, "X.txt")
    writedlm(Xfile, X')
    yfile = joinpath(dir, "Y.txt")
    writedlm(yfile, Y)
    Xtfile = joinpath(dir, "X_t.txt")
    writedlm(Xtfile, X_t')
    ytfile = joinpath(dir, "Y_t.txt")
    writedlm(ytfile, Y_t)

    Nk = size(X, 1)

    Random.seed!(6480000951 + seed * 10^5)
    if algo == :lal
        m = Comm(Nk, K)
        opt = LAL(η)
    else
        m = Chain(
            Fullβ(Nk, K, β = β),
            x->vec(sum(x, dims=1) ./ √K)
           )
        opt = Descent(η)
    end

    @show accuracy(m, X, Y)
    @show accuracy(m, X_t, Y_t)

    eps1 = 1

    open(outfile, "w") do f
        println(f, "# η=$η β₀=$β γ₀=$γ β₁=$β₁ γ₁=$γ₁")
    end

    setup = Setup(X, Y, X_t, Y_t, B, m, loss, opt,
                  γ, β₁, γ₁, eps1, dir, outfile, epochs,
                  stop_at_sol, loss_threshold, id)

    return run!(setup)
end

function run!(setup::Setup; st0 = 0)
    @extract setup : X Y X_t Y_t B m loss opt γ β₁ γ₁ eps1 dir outfile epochs stop_at_sol loss_threshold

    converged = false
    solved = false
    for st = (1:epochs) .+ st0
        @info "γ=$γ β=$(m.layers[1].β)"
        renorm!(m)

        M = length(Y)
        p = randperm(M)
        r(b) = (1:B) .+ (b-1)*B
        indata = [X[:, p[r(b)]] for b = 1:(M÷B)]
        labels = [Y[p[r(b)]] for b = 1:(M÷B)]

        result = etrain!((eps1,1),
                         (x,y)->loss(m, x, y, γ),
                         params(m), (indata, labels),
                         opt,
                         ep0 = (st-1) * eps1,
                         tstcallback = ep->callback(ep, m, X, Y, X_t, Y_t, γ, stop_at_sol=stop_at_sol),
                         outf = outfile,
                         loss_threshold = loss_threshold)
        m.layers[1].β *= β₁
        γ *= γ₁
        if result == :converged
            @info "CONVERGED"
            converged = true
            break
        elseif result == :stopped
            break
        end
    end
    _, (trnacc, s_trnacc, tstacc, s_tstacc) = callback(0, m, X, Y, X_t, Y_t, γ)
    solved = s_trnacc == 1
    @info "acc = $s_trnacc s_acc = $s_trnacc"
    solved && @info "SOLVED"

    dumpweights(m, dir)

    return converged, s_trnacc, s_tstacc, setup
end

function run!(setup::Setup{<:Comm,LAL}; st0 = 0)
    @extract setup : X Y X_t Y_t B m loss opt eps1 dir outfile epochs loss_threshold

    @assert loss == loss_lal
    @assert eps1 == 1

    @extract m : Ws K

    solved = false
    converged = false
    for st = (1:epochs) .+ st0
        renorm!(m)

        M = length(Y)
        p = randperm(M)
        r(b) = (1:B) .+ (b-1)*B
        indata = [X[:, p[r(b)]] for b = 1:(M÷B)]
        labels = [Y[p[r(b)]] for b = 1:(M÷B)]

        l = 0.0
        errs = 0
        M = 0
        for (x, y) in zip(indata, labels)
            B = length(y)
            M += B
            σ = m(x)
            h = m.h
            for a = 1:B
                σ[a] * y[a] > 0 && continue
                errs += 1

                kₘ = 0
                δₘ = eltype(h)(-Inf)
                for k = 1:K
                    δ = h[k,a] * y[a]
                    δ > 0 && continue
                    if δ > δₘ
                        kₘ = k
                        δₘ = δ
                    end
                end
                l -= δₘ
                Ws[kₘ] .+= sum((opt.η * y[a]) .* x[:,a], dims=2)'
            end
        end
        l /= M
        acc = 1 - errs / M
        tstacc = accuracy(m, X_t, Y_t)
        outstr = "  epoch $st: loss=$l ($acc $tstacc)"
        println(outstr)
        outfile ≠ nothing && open(f->println(f, outstr), outfile, "a")

        if l ≤ loss_threshold
            @info "CONVERGED"
            converged = true
            break
        elseif acc == 1
            break
        end
    end
    acc = accuracy(m, X, Y)
    tstacc = accuracy(m, X_t, Y_t)
    solved = acc == 1
    @info "acc = $acc tstacc = $tstacc"
    solved && @info "SOLVED"

    dumpweights(m, dir)

    return converged, acc, tstacc, setup
end

function dumpweights(m::Chain, dir::String)
    Wfile = joinpath(dir, "W.txt")
    writedlm(Wfile, vcat((W.data for W in m.layers[1].Ws)...))
end

function dumpweights(m::Comm, dir::String)
    Wfile = joinpath(dir, "W.txt")
    writedlm(Wfile, vcat(m.Ws...))
end

function comp_hessian(setup::Setup; kw...)
    @extract setup : X Y m dir
    Ws = m isa Comm ? m.Ws : [W.data for W in m.layers[1].Ws]
    return comp_hessian(X, Y, Ws, dir; kw...)
end

function comp_hessian(X, Y, Ws, dir::String; β = 10.0, γ = 1.0, correction = false)
    Hfile = joinpath(dir, "hess.beta_$β.gamma_$γ.corr_$correction.txt")
    if isfile(Hfile)
        @info reading hessian from file
        H = readdlm(Hfile)
    else
        H = HardcodedHessians.hess_treecomm_mse(Ws, X, Y, β, γ, correction=correction)
        writedlm(Hfile, H)
    end

    evfile = joinpath(dir, "eigvals.beta_$β.gamma_$γ.corr_$correction.txt")
    if isfile(evfile)
        @info "reading ev's from file"
        ev = readdlm(evfile)
    else
        ev = sort!(eigvals(H), rev=true)
        writedlm(evfile, ev)
    end

    return H, ev
end

function seek_minβ(setup::Setup; kw...)
    @extract setup: X Y m
    return seek_minβ(X, Y, m; kw...)
end

seek_minβ(X, Y, m::Comm; kw...) = seek_minβ(X, Y, chain(m); kw...)

function seek_minβ(X, Y, m::Chain; tol = 0.5)
    curr_β = m.layers[1].β
    _, (trnacc, s_trnacc, _, _) = callback(0, m, X, Y, X, Y, 1.0)
    # @info "acc = ($trnacc, $s_trnacc)"
    if trnacc ≥ s_trnacc
        β₁ = curr_β
        β₀ = 1e-10
    else
        β₁ = 1e10
        β₀ = curr_β
    end
    δ = β₁ - β₀
    while δ > tol
        βₘ = √(β₁ * β₀)
        # @info "β₉,β₁ = $β₀,$β₁ βₘ = $βₘ"
        m.layers[1].β = βₘ
        trnacc = data(accuracy(m, X, Y))
        # @info "  acc = $trnacc"
        if trnacc ≥ s_trnacc
            β₁ = βₘ
        else
            β₀ = βₘ
        end
        δ = β₁ - β₀
    end
    m.layers[1].β = curr_β
    @info "minβ = $β₁"
    return β₁
end

end # module FullCommSGD_FMNIST
