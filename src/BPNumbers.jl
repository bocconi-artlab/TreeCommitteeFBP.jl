module BPNumbers

export ⊗, ⊘, ↑, msg_add, msg_minus, msg_times

function ⊗ end
function ⊘ end
function ↑ end
const msg_add = ⊗
const msg_minus = ⊘
const msg_times = ↑

end # module
