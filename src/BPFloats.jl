# This file is a part of TreeCommitteeFBP.jl. License is MIT: http://github.com/carlobaldassi/TreeCommitteeFBP.jl/LICENCE.md

module BPFloats

export BPFloat64, fflatp, fdamp

import Base: convert, show, zero
import ..BPNumbers: ⊗, ⊘, ↑

primitive type BPFloat64 <: AbstractFloat 64 end

convert(::Type{BPFloat64}, x::Float64) = reinterpret(BPFloat64, x)
convert(::Type{Float64}, x::BPFloat64) = reinterpret(Float64, x)

BPFloat64(x::Float64) = convert(BPFloat64, x)
Float64(x::BPFloat64) = convert(Float64, x)

show(io::IO, x::BPFloat64) = show(io, Float64(x))

Base.:(==)(a::BPFloat64, b::BPFloat64) = (Float64(a) == Float64(b))

⊗(a::BPFloat64, b::BPFloat64) = BPFloat64(Float64(a) + Float64(b))
⊘(a::BPFloat64, b::BPFloat64) = BPFloat64(Float64(a) - Float64(b))

↑(a::BPFloat64, x::Real) = BPFloat64(Float64(a) * x)

zero(::Type{BPFloat64}) = BPFloat64(0.0)

fflatp(n::Int) = fill(zero(BPFloat64), n)

function fdamp(newx::BPFloat64, oldx::BPFloat64, ψ::Float64)
    return BPFloat64((1 - ψ) * Float64(newx) + ψ * Float64(oldx))
end

end # module

