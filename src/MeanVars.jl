# This file is a part of TreeCommitteeFBP.jl. License is MIT: http://github.com/carlobaldassi/TreeCommitteeFBP.jl/LICENCE.md

module MeanVars

using ExtractMacro

export MeanVar, mvflatp, mvdamp, logz, loghu, overlap,
       squareddiff, repl_overlap

import Base: convert, float, show, zero, *
import ..BPNumbers: ⊗, ⊘, ↑

struct MeanVar
    λμ::Float64 # mean × variance⁻¹
    λ::Float64  # variance⁻¹
end

MeanVar(χ) = MeanVar(0.0, χ)

convert(::Type{MeanVar}, x::Float64) = MeanVar(x)

function convert(::Type{Tuple{Float64,Float64}}, x::MeanVar)
    @extract x : λμ λ
    v = 1 / λ
    if λ == 0
        @assert λμ == 0
        μ = 0.0
    else
        μ = λμ / λ
    end
    return μ, v
end

float(x::MeanVar) = convert(Tuple{Float64,Float64}, x)

show(io::IO, x::MeanVar) = show(io, "MV$(float(x))")

Base.:(==)(a::MeanVar, b::MeanVar) = (a.λμ == b.λμ && a.λ == b.λ)

⊗(a::MeanVar, b::MeanVar) = MeanVar(a.λμ + b.λμ, a.λ + b.λ)
⊘(a::MeanVar, b::MeanVar) = MeanVar(a.λμ - b.λμ, a.λ - b.λ)

# pass h(x1) through an interaction exp(-γ/2 * (x1-x2)^2)
# and get u(x2)
function (*)(h::MeanVar, γ::Float64)
    γ == 0 && return MeanVar(0.0, 0.0)
    f = 1 / (1 + h.λ / γ)
    return MeanVar(h.λμ * f, h.λ * f)
end

↑(a::MeanVar, x::Real) = MeanVar(a.λμ * x, a.λ * x)

zero(::Type{MeanVar}) = MeanVar(0.0)

mvflatp(n::Int, x::MeanVar = zero(MeanVar)) = fill(x, n)
mvflatp(n::Int, x::Float64) = fill(MeanVar(x), n)


function mvdamp(newx::MeanVar, oldx::MeanVar, ψ::Float64)
    return MeanVar((1 - ψ) * newx.λμ + ψ * oldx.λμ,
                   (1 - ψ) * newx.λ + ψ * oldx.λ)
end

# ∫dx ∏ₐ uₐᵢ(x)
function logz(m::MeanVar)
    return -log(m.λ) / 2 + 1/2 * (m.λμ)^2 / m.λ + log(2π) / 2
end
logz(us::Vector{MeanVar}) = logz(reduce(⊗, us, init=zero(MeanVar)))

# ∫dx h(x)*u(x)
# note that this is asymmetric: h is considered normalized while u is not
# it can also be obtained as: logz(h ⊗ u) - logz(h)
function loghu(h::MeanVar, u::MeanVar)
    @extract h : λμh=λμ λh=λ
    @extract u : λμu=λμ λu=λ

    return -log((λh + λu) / λh) / 2 + (λμh * λμu) / (λh + λu) + (λh * λμu^2 - λu * λμh^2) / (2 * λh * (λh + λu))
end

# mean of x1*x2 with interaction term -γ/2 * (x1 - x2)^2
# ∫dx1 dx2 x1*x2 * h1(x1) * h2(x2) * exp(-γ/2 * (x1 - x2)^2)
function overlap(a::MeanVar, b::MeanVar, γ::Float64)
    @extract a : λμ1=λμ λ1=λ
    @extract b : λμ2=λμ λ2=λ

    if γ == Inf
        den = λ1 + λ2
        c = λμ1 + λμ2
        return c^2 / den^2 + 1 / den
    end

    den = λ1 * λ2 + γ * (λ1 + λ2)
    c = γ * (λμ1 + λμ2)
    n1 = λ2 * λμ1 + c
    n2 = λ1 * λμ2 + c
    return (n1 * n2) / den^2 + γ / den
end

# mean of (x1-x2)^2 with interaction term -γ/2 * (x1 - x2)^2
# ∫dx1 dx2 (x1-x2)^2 * h1(x1) * h2(x2) * exp(-γ/2 * (x1 - x2)^2)
function squareddiff(a::MeanVar, b::MeanVar, γ::Float64)
    γ == Inf && return 0.0

    μ1, v1 = float(a)
    μ2, v2 = float(b)

    den = 1 + γ * (v1 + v2)
    return ((μ1 - μ2) / den)^2 + (v1 + v2) / den
end

# mean of x1*x2 with indirect interaction terms through xs: -γ/2 * (x[12] - xs)^2
# assuming x1 and x2 have the same field h and xs has another field hs
# ∫dx1 dx2 dxs x1*x2 * h(x1) * h(x2) * hs(xs) * exp(-γ/2 * (x1 - xs)^2) * exp(-γ/2 * (x2 - xs)^2)
function repl_overlap(h::MeanVar, hs::MeanVar, γ::Float64)
    @extract h  : λμ λ
    @extract hs : λμs=λμ λs=λ

    if γ == Inf
        den = 2λ + λs
        c = 2λμ + λμs
        return (c / den)^2 + 1 / den
    end

    den = 2 * γ * λ + (γ + λ) * λs
    c = λs * λμ + γ * (2λμ + λμs)
    return (c / den)^2 + (γ^2 / (γ + λ)) / den
end


end # module

