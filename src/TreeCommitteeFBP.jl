# This file is a part of TreeCommitteeFBP.jl. License is MIT: http://github.com/carlobaldassi/TreeCommitteeFBP.jl/LICENCE.md

module TreeCommitteeFBP

export focusingBP, weight_enum, MagT64, MagP64,
       read_messages, write_messages,
       FocusingProtocol, StandardReinforcement, Scoping, PseudoReinforcement, FreeScoping

using StatsFuns
using GZip
using ExtractMacro
using SpecialFunctions
using Printf, LinearAlgebra, Random

include("BPNumbers.jl")
using .BPNumbers

include("Magnetizations.jl")
using .Magnetizations

include("MeanVars.jl")
using .MeanVars

include("Util.jl")
using .Util

const vmin = 1e-2

struct Messages{F<:Mag64}
    M::Int
    N::Int
    K::Int

    # 2 layers:
    #   0  --> external fields
    #   1  --> theta (perceptron) nodes with cont. weights
    #   2  --> theta (perceptron) nodes with weights fixed to 1
    #
    # notation:
    #   m* --> total fields (includes external fields)
    #   u* --> messages directed down (node->variable)
    #   U* --> messages directed up (node->variable)
    #
    # variable names (quantity):
    #   w  --> weights (K × N/K)
    #   τ1 --> outputs from the first layer of perceptrons (KxM)
    #          also inputs to the second layer
    #   τ2 --> outputs from the second layer of perceptrons (M)
    #
    #                DEPTH
    ux::MVVec2       # 0     # field from replicas
    up::MVVec2       # 0     # fixed polarizing field on w
    mw::MVVec2       # 0+1
    mτ1::MagVec2{F}  # 1+2
    uw::MVVec3       # 1
    Uτ1::MagVec2{F}  # 1
    mτ2::MagVec{F}   # 2+
    uτ1::MagVec2{F}  # 2
    Uτ2::MagVec{F}   # 2

    function Messages{F}(M::Int, N::Int, K::Int, ux::MVVec2, up::MVVec2, mw::MVVec2, mτ1::MagVec2{F},
                         uw::MVVec3, Uτ1::MagVec2{F}, mτ2::MagVec{F}, uτ1::MagVec2{F}, Uτ2::MagVec{F};
                         check::Bool = true) where {F<:Mag64}
        @assert N % K == 0
        Nk = N ÷ K
        if check
            checkdims(ux, K, Nk)
            checkdims(up, K, Nk)
            checkdims(mw, K, Nk)
            checkdims(mτ1, M, K)
            checkdims(uw, M, K, Nk)
            checkdims(Uτ1, M, K)
            checkdims(mτ2, M)
            checkdims(uτ1, M, K)
            checkdiUs(mτ2, M)
        end
        new(M, N, K, ux, up, mw, mτ1, uw, Uτ1, mτ2, uτ1, Uτ2)
    end

end

function Messages(::Type{F}, M::Integer, N::Integer, K::Integer; x::Float64 = 0.0) where {F<:Mag64}
    @assert N % K == 0
    Nk = N ÷ K
    χ = 1.0
    ux = [mvflatp(Nk) for k = 1:K]
    up = [mvflatp(Nk) for k = 1:K]
    mw = [mvflatp(Nk, χ) for k = 1:K]
    mτ1 = [mflatp(F, K) for a = 1:M]
    randu() = (ρ = (2 * rand() - 1); MeanVar(x * ρ, x))
    uw = [[[randu() for i = 1:Nk] for k = 1:K] for a = 1:M]
    # uw = [[mvflatp(Nk) for k = 1:K] for a = 1:M]
    Uτ1 = [mflatp(F, K) for a = 1:M]
    mτ2 = mflatp(F, M)
    uτ1 = [mflatp(F, K) for a = 1:M]
    Uτ2 = mflatp(F, M)

    for k = 1:K, i = 1:Nk
        mw[k][i] = mw[k][i] ⊗ ux[k][i] ⊗ up[k][i]
    end
    for k = 1:K, i = 1:Nk, a = 1:M
        mw[k][i] = mw[k][i] ⊗ uw[a][k][i]
    end
    for a = 1:M, k = 1:K
        mτ1[a][k] = mτ1[a][k] ⊗ Uτ1[a][k] ⊗ uτ1[a][k]
    end

    return Messages{F}(M, N, K, ux, up, mw, mτ1, uw, Uτ1, mτ2, uτ1, Uτ2, check=false)
end

Messages(::Type{F}, messages::Messages{F}) where {F<:Mag64} = messages
function Messages(::Type{F}, messages::Messages) where {F<:Mag64}
    @extract messages : M N K ux up mw mτ1 uw Uτ1 mτ2 uτ1
    return Messages{F}(M, N, K,
                       ux, up, mw, chgeltype(mτ1, F), uw,
                       chgeltype(Uτ1, F), chgeltype(mτ2, F), chgeltype(uτ1, F), chgeltype(Uτ2, F),
                       check=false)
end

# TODO: fix this
# function read_messages(io::IO, ::Type{F}) where {F<:Mag64}
#     l = split(readline(io))
#     length(l) == 2 && l[1] == "fmt:" || error("invalid messages file")
#     fmt = Val{Symbol(l[2])}
#     l = split(readline(io))
#     length(l) == 4 && l[1] == "N,K,M:" || error("invalid messgaes file")
#     N, K, M = parse(Int, l[2]), parse(Int, l[3]), parse(Int, l[4])
#
#     ux = [mflatp(F, N) for k = 1:K]
#     up = [mflatp(F, N) for k = 1:K]
#     mw = [mflatp(F, N) for k = 1:K]
#     mτ1 = [mflatp(F, K) for a = 1:M]
#     uw = [[mflatp(F, N) for k = 1:K] for a = 1:M]
#     Uτ1 = [mflatp(F, K) for a = 1:M]
#     mτ2 = mflatp(F, M)
#     uτ1 = [mflatp(F, K) for a = 1:M]
#     Uτ2 = mflatp(F, M)
#
#     expected_lines = K + M + M*K + M + 1 + M + K
#     for (i,l) in enumerate(eachline(io))
#         i > expected_lines && (strip(l) == "END" || error("invalid messages file"); break)
#         @readmagvec(l, fmt, ux, up, mw, mτ1, uw, Uτ1, mτ2, uτ1, Uτ2)
#     end
#     eof(io) || error("invalid messages file")
#     return Messages{F}(M, N, K, ux, up, mw, mτ1, uw, Uτ1, mτ2, uτ1, Uτ2, check=false)
# end
#
# """
#     read_messages(filename, mag_type)
#
# Reads messages from a file. `mag_type` is the internal storage format used in the resulting `Messages` object,
# it can be either `MagT64` (uses tanhs, accurate but slower) or `MagP64` (plain format, faster but inaccurate).
#
# The file format is the one produced by [`write_messages`](@ref).
# """
# read_messages(filename::AbstractString, ::Type{F}) where {F<:Mag64} = gzopen(io->read_messages(io, F), filename, "r")
#
#
# """
#     write_messages(filename, messages)
#
# Writes messages to a file. The messages can be read back with [`read_messages`](@ref). Note that the output
# is a plain text file compressed with gzip.
# """
# function write_messages(filename::AbstractString, messages::Messages)
#     gzopen(filename, "w") do f
#         write_messages(f, messages)
#     end
# end
#
# function write_messages(io::IO, messages::Messages{F}) where {F<:Mag64}
#     @extract messages : N K M ux up mw mτ1 uw Uτ1 mτ2 uτ1 Uτ2
#
#     println(io, "fmt: ", magformat(F))
#     println(io, "N,K,M: $N $K $M")
#     @dumpmagvecs(io, ux, up, mw, mτ1, uw, Uτ1, mτ2, uτ1, Uτ2)
#     println(io, "END")
# end

Base.eltype(messages::Messages{F}) where {F<:Mag64} = F

function copyto!(dest::Messages{F}, src::Messages{F}) where {F<:Mag64}
    dest.N == src.N || throw(ArgumentError("incompatible arguments: dest.N=$(dest.N) src.N=$(src.N)"))
    dest.K == src.K || throw(ArgumentError("incompatible arguments: dest.K=$(dest.K) src.K=$(src.K)"))
    dest.M == src.M || throw(ArgumentError("incompatible arguments: dest.M=$(dest.M) src.M=$(src.M)"))
    for k = 1:dest.K
        copyto!(dest.ux[k], src.ux[k])
        copyto!(dest.up[k], src.up[k])
        copyto!(dest.mw[k], src.mw[k])
    end
    for a = 1:dest.M, k = 1:dest.K
        copyto!(dest.uw[a][k], src.uw[a][k])
    end
    for a = 1:dest.M
        copyto!(dest.mτ1[a], src.mτ1[a])
        copyto!(dest.Uτ1[a], src.Uτ1[a])
        copyto!(dest.uτ1[a], src.uτ1[a])
    end
    copyto!(dest.mτ2, src.mτ2)
    copyto!(dest.Uτ2, src.Uτ2)
    return dest
end

function set_outfields!(messages::Messages{F}, output::Vector, β::Float64) where {F<:Mag64}
    @extract messages : N K M mτ2
    @assert length(output) == M
    t = tanh(β / 2)
    for a = 1:M
        mτ2[a] = forcedmag(F, output[a] * t) # forced avoids clamping
    end
end

function set_pfields!(messages::Messages{F}, wp::Vector, γ::Float64) where {F<:Mag64}
    @extract messages : N K M mw up
    @assert length(wp) == N
    z = norm(wp) / √N
    Nk = N ÷ K
    for k = 1:K, i = 1:Nk
        oldup = up[k][i]
        up[k][i] = MeanVar(γ * wp[(k-1)*Nk + i] / z, γ)
        mw[k][i] = mw[k][i] ⊘ oldup ⊗ up[k][i]
    end
end

mutable struct Params
    damping::Float64
    ϵ::Float64
    β::Float64
    max_iters::Int
    accuracy1::Symbol
    accuracy2::Symbol
    χ::MeanVar
    χst::MeanVar
    r::Float64
    γ::Float64
    # dγ::Float64
    quiet::Bool
    κ::Int
end

print_marginals(messages::Messages, params::Params) = print_marginals(stdout, messages, params)
function print_marginals(io::IO, messages::Messages, params::Params)
    @extract messages : N K mw
    for k = 1:K, i = 1:N
        μ, v = float(mw[k][i])
        @printf(io, "%i %i %.15f %.15f\n", k, i, μ, v)
    end
end


struct Patterns
    M::Int
    X::Vec2
    output::IVec
    teacher::Union{Nothing,Vec2}
    function Patterns(X::AbstractVector, output::AbstractVector, teacher::Union{Nothing,Vec2} = nothing; check::Bool = true)
        M = length(X)
        if check
            length(output) == M || throw(ArgumentError("incompatible lengths of inputs and outputs: $M vs $(length(output))"))
            all(ξ->all(ξi->abs(ξi) == 1, ξ), X) || throw(ArgumentError("inputs must be ∈ {-1,1}"))
            all(o->abs(o) == 1, output) || throw(ArgumentError("outputs must be ∈ {-1,1}"))
            if teacher ≢ nothing
                K = length(teacher)
                K > 0 || throw(ArgumentError("empty teacher"))
                allNk = unique(length.(teacher))
                length(allNk) == 1 || throw(ArgumentError("uneven teacher, found hidden units of length: $(allNk)"))
                Nk = allNk[1]
                N = K * Nk
                wrongout = 0
                for (a, ξ) in enumerate(X)
                    length(ξ) == N || throw(ArgumentError("patterns incompatible with teacher, teacher size=$N pattern size=$N"))
                    s = 0.0
                    for k = 1:K
                        rng = ((k-1) * Nk + 1):(k*Nk)
                        s += sign(teacher[k] ⋅ (@view ξ[rng]))
                    end
                    sign(s) != output[a] && (wrongout += 1)
                end
                wrongout > 0 && @warn "outputs incompatible with teacher, found $wrongout errors"
            end
        end
        new(M, X, output, teacher)
    end
end
Patterns(Xo::Tuple{Vec2,Vec}) = Patterns(Xo...)

@doc """
    Patterns(inputs::AbstractVector, outputs::AbstractVector)

Construct a `Patterns` object. `inputs` and `outputs` must have the same length.
`outputs` entries must be ∈ {-1,1}. `intputs` entries must be Vectors in which
each element is ∈ {-1,1}, and all the vectors must have the same length.
""" Patterns(inputs::AbstractVector, outputs::AbstractVector)


"""
    Patterns(NM::Tuple{Integer,Integer}; teacher::Union{Bool,Int,Vector{Vector{Float64}}} = false)

Constructs random (unbiasad, unifom i.i.d.) binary patterns. The `NM` argument is a Tuple with
two items, the size of the inputs (`N`) and the number of patterns (`M`).
The keyword argument `teacher` controls how the outputs are generated:
* If `false`, they are random i.i.d. (the default)
* If `true`, a teacher unit with a single hidden layer (a perceptron) is generated randomly
  and it's used to compute the outputs
* If an `Int` is given, it's the number of hidden units of the teacher, which is generated
  randomly and used to compute the outputs
* If a `Vector{Vector{Float64}}` is given, it represents the weights of the teacher, with one
  `Vector{Float64}` for each hidden unit.
"""
function Patterns(NM::Tuple{Integer,Integer}; teacher::Union{Bool,Int,Vec2} = false)
    N, M = NM
    X = [rand(-1.0:2.0:1.0, N) for a = 1:M]
    if teacher == false
        output = ones(M)
    else
        if !isa(teacher, Vec2)
            K::Int = isa(teacher, Bool) ? 1 : teacher
            N % K == 0 || throw(ArgumentError("N must be divisible by K: N=$N K=$K"))
            Nk = N ÷ K
            tw = [randn(Nk) for k = 1:K]
            # tw = [ones(Nk) for k = 1:K]
            map!(w->w./sum(w.^2), tw, tw)
        else
            K = length(teacher)
            K > 0 || throw(ArgumentError("empty teacher"))
            N % K == 0 || throw(ArgumentError("N must be divisible by K: N=$N K=$K"))
            Nk = N ÷ K
            tw = teacher
            all(w -> length(w) == Nk, tw) || throw(ArgumentError("invalid teacher length, expected $Nk, given: $(sort!(unique(length.(tw))))"))
        end
        output = Array{Float64}(undef, M)
        for (a, ξ) in enumerate(X)
            s = 0.0
            for k = 1:K
                rng = ((k-1) * Nk + 1):(k*Nk)
                s += sign(tw[k] ⋅ (@view ξ[rng]))
            end
            output[a] = sign0(s)
        end
        @assert all(o->abs(o) == 1, output)
    end
    Patterns(X, output, teacher == false ? nothing : tw, check=false)
end
Patterns(patterns::Patterns) = deepcopy(patterns)

"""
    Patterns(patternsfile::AbstractString)

Read patterns from a file. It only reads the inputs, one pattern per line, entries separated by
whitespace. All lines must have the same length. All outputs are assumed to be `1`. The file may
optionally be gzipped.
"""
function Patterns(patternsfile::AbstractString)
    X = Vec[]
    N = 0
    M = 0
    gzopen(patternsfile) do f
        M = 0
        for l in eachline(f)
            push!(X, map(x->parse(Float64, x), split(l)))
            M += 1
        end
    end
    o = Int[1.0 for a = 1:M]

    return Patterns(X, o)
end

function computeσ²(w::Vec)
    σ² = 0.0
    @inbounds @simd for wi in w
        # σ² += (1 - wi^2)
        σ² += (1 - wi*wi)
    end
    return σ²
end

# function computeσ²(w::Vec, ξ::Vec)
#     σ² = 0.0
#     @inbounds for (wi,ξi) in zip(w,ξ)
#         σ² += (1 - wi^2) * ξi^2
#     end
#     return σ²
# end

computeσ(σ²::Float64) = √(2σ²)
computeσ(w::Vec) = √(2computeσ²(w))
# computeσ(w::Vec, ξ::Vec) = √(2computeσ²(w, ξ))

gauss(x, dσ²) = ℯ^(-x^2 / dσ²) / √(π * dσ²)
gauss(x) = ℯ^(-x^2 / 2) / √(2π)

function subfield!(h::XVec, m::XVec, u::XVec) where {XVec <: Union{MagVec,MVVec}}
    @inbounds @simd for i = 1:length(m)
        h[i] = m[i] ⊘ u[i]
    end
end

function addfield!(m::XVec, h::XVec, u::XVec) where {XVec <: Union{MagVec,MVVec}}
    @inbounds @simd for i = 1:length(m)
        m[i] = h[i] ⊗ u[i]
    end
end

let hsT = Dict{Int,MagVec{MagT64}}(), hsP = Dict{Int,MagVec{MagP64}}(), hsA = Dict{Int,MVVec}(),
    vhs = Dict{Int,Vec}(), vhvs = Dict{Int,Vec}(),
    Cs = Dict{Int,Tuple{Vec2,Vec2}}()

    geth(::Type{MagT64}, N::Int) = get!(hsT, N) do; Array{MagT64}(undef, N) end
    geth(::Type{MagP64}, N::Int) = get!(hsP, N) do; Array{MagP64}(undef, N) end
    geth(::Type{MeanVar}, N::Int) = get!(hsA, N) do; Array{MeanVar}(undef, N) end
    getvh(N::Int) = get!(vhs, N) do; Array{Float64}(undef, N) end
    getvhv(N::Int) = get!(vhvs, N) do; Array{Float64}(undef, N) end
    getC(N::Int) = get!(Cs, N) do; ([zeros(i+1) for i = 1:N], [zeros((N-i+1)+1) for i = 1:N]) end

    global function theta_node_update_approx!(m::MagVec{F}, M::F, ξ::Vec, u::MagVec{F}, U::F, params::Params, κ::Int) where {F<:Mag64}
        @extract params : λ=damping

        @assert κ == 0 # robustness, TODO

        N = length(m)
        h::MagVec{F} = geth(F, N)
        vh = getvh(N)

        subfield!(h, m, u)
        H = M ⊘ U

        @inbounds for i = 1:N
            vh[i] = h[i]
        end

        vH = Float64(H)
        # σ² = computeσ²(vh, ξ)
        σ² = computeσ²(vh) # assume ξ = ±1

        μ = vh ⋅ ξ

        dσ² = 2σ²
        newU = merf(F, μ / √dσ²)

        maxdiff = abs(U - newU)
        U = damp(newU, U, λ)
        newM = H ⊗ U
        M = newM

        g = gauss(μ, dσ²)

        p0 = 2vH * g / (1 + vH * U)

        pμ = p0 * (p0 + μ / σ²)

        pσ = p0 * (1 - μ / σ² - μ * p0) / dσ²

        @inbounds for i = 1:N
            ξi = ξ[i]
            hi = vh[i]
            newu = convert(F, clamp(ξi * (p0 + ξi * (hi * pμ + ξi * (1-hi^2) * pσ)), -1+3e-16, 1-3e-16)) # use mag-functions?
            d = conv_diff(newu, u[i])
            maxdiff = ifelse(maxdiff < d, d, maxdiff)
            u[i] = damp(newu, u[i], λ)
        end
        addfield!(m, h, u)

        return maxdiff, U, M
    end

    global function theta_node_update_accurate!(m::MagVec{F}, M::F, ξ::Vec, u::MagVec{F}, U::F, params::Params, κ::Int) where {F<:Mag64}
        @extract params : λ=damping

        @assert κ == 0 # robustness, TODO

        N = length(m)
        h::MagVec{F} = geth(F, N)
        vh = getvh(N)

        subfield!(h, m, u)
        H = M ⊘ U

        @inbounds for i = 1:N
            vh[i] = h[i]
        end

        # σ² = computeσ²(vh, ξ)
        σ² = computeσ²(vh) # assume ξ = ±1
        μ = vh ⋅ ξ

        dσ² = 2σ²
        newU = merf(F, μ / √dσ²)

        maxdiff = 0.0
        U = damp(newU, U, λ)
        M = H ⊗ U

        @inbounds for i = 1:N
            ξi = ξ[i]
            hi = vh[i]
            μ̄ = μ - ξi * hi
            σ̄² = σ² - (1-hi^2) # * ξi^2 # assume ξ = ±1
            sdσ̄² = √(2σ̄²)
            m₊ = (μ̄ + ξi) / sdσ̄²
            m₋ = (μ̄ - ξi) / sdσ̄²
            newu = erfmix(H, m₊, m₋)
            d = abs(newu - u[i])
            maxdiff = ifelse(maxdiff < d, d, maxdiff)
            u[i] = damp(newu, u[i], λ)
        end
        addfield!(m, h, u)
        return maxdiff, U, M
    end

    function theta_node_update_approx!(m::MVVec, M::F, ξ::Vec, u::MVVec, U::F, params::Params) where {F<:Mag64}
        @extract params : λ=damping

        # @info "APPROX"

        N = length(m)
        h::MVVec = geth(MeanVar, N)
        vh = getvh(N)
        vhv = getvhv(N)

        subfield!(h, m, u)
        H = M ⊘ U

        # warns = 0
        @inbounds for i = 1:N
            vh[i], vhv[i] = float(h[i])
            @assert vhv[i] ≥ 0
        end
        # warns > 0 && @warn "warns = $warns"

        μ = vh ⋅ ξ
        σ² = sum(vhv) # assume ξ = ±1
        @assert σ² ≥ 0 "σ² = $σ² !!!"

        dσ² = 2σ²
        newU = merf(F, μ / √dσ²)

        maxdiff = 0.0
        U = damp(newU, U, λ)
        M = H ⊗ U

        σ = √σ²
        μr = μ / σ

        ω = ℯ^(-μr^2 / 2) / √(2π)
        if 1 - abs(H) < 1e-10 && sign(H) * μr < -6
            c = 1 / ((1 - abs(H)) / (H * ω) - 2 / μr + 2 / μr^3 - 6 / μr^5)
        else
            η = erf(μr / √2)
            c = H * ω / (1 + H * η)
        end


        λμ₀ = 2c
        λμ₁ = 2c * (2c + μr)
        λμh₂ = c * ((2c + μr) * (4c + μr) - 1)
        λμv₂ = c * (1 - μr * (2c + μr))

        λ₀ = 4c^2 + 2μr * c
        λ₁ = 2c * ((2c + μr) * (4c + μr) - 1)
        λh₂ = c * (μr^3 + 14μr^2 * c + 48μr * c^2 - 3μr + 48c^3 - 8c)
        λv₂ = c * (-μr^3 - 6μr^2 * c - 8μr * c^2 + 3μr + 4c)

        @inbounds for i = 1:N
            ξi = ξ[i]
            hi = vh[i]
            vi = vhv[i]

            ξr = ξi / σ

            newuλμ = ξr * (λμ₀ + λμ₁ * hi * ξr + λμh₂ * hi^2 * ξr^2 + λμv₂ * vi * ξr^2)
            newuλ = ξr^2 * (λ₀ + λ₁ * hi * ξr + λh₂ * hi^2 * ξr^2 + λv₂ * vi * ξr^2)

            newu = MeanVar(newuλμ, newuλ)
            # @show newu
            d = max(abs(newu.λμ - u[i].λμ),
                    abs(newu.λ - u[i].λ))
            # @show newu, u[i]
            # @show ξi * Float64(newu), ξi * Float64(u[i])
            maxdiff = ifelse(maxdiff < d, d, maxdiff)
            u[i] = mvdamp(newu, u[i], λ) # TODO: merge damp and fdamp
        end
        addfield!(m, h, u)
        return maxdiff, U, M
    end

    function theta_node_update_accurate!(m::MVVec, M::F, ξ::Vec, u::MVVec, U::F, params::Params) where {F<:Mag64}
        @extract params : λ=damping

        # @info "ACCURATE"

        N = length(m)
        h::MVVec = geth(MeanVar, N)
        vh = getvh(N)
        vhv = getvhv(N)

        subfield!(h, m, u)
        H = M ⊘ U

        # warns = 0
        @inbounds for i = 1:N
            vh[i], vhv[i] = float(h[i])
            @assert vhv[i] ≥ 0
        end
        # warns > 0 && @warn "warns = $warns"

        μ = vh ⋅ ξ
        σ² = sum(vhv) # assume ξ = ±1
        @assert σ² ≥ 0 "σ² = $σ² !!!"

        dσ² = 2σ²
        newU = merf(F, μ / √dσ²)

        maxdiff = 0.0
        U = damp(newU, U, λ)
        M = H ⊗ U

        # @show M,H,newU,U

        @inbounds for i = 1:N
            ξi = ξ[i]
            hi = vh[i]
            vi = vhv[i]
            μ̄ = μ - ξi * hi
            σ̄² = σ² - vi # assume ξ = ±1
            μr = μ̄ / √(σ̄²)
            ξr = ξi / √(σ̄²)
            # @show μ̄, σ̄², x, ξi*x
            ω = ℯ^(-μr^2 / 2) / √(2π)
            if 1 - abs(H) < 1e-10 && sign(H) * μr < -6
                c = 1 / ((1 + sign(μr) * H) / (H * ω) - 2 / μr + 2 / μr^3 - 6 / μr^5)
            else
                η = erf(μr / √2)
                c = H * ω / (1 + H * η)
            end
            newuλμ = 2ξr * c
            newuλ = newuλμ^2 + 2 * μr * ξr^2 * c
            newu = MeanVar(newuλμ, newuλ)
            # @show newu
            d = max(abs(newu.λμ - u[i].λμ),
                    abs(newu.λ - u[i].λ))
            # @show newu, u[i]
            # @show ξi * Float64(newu), ξi * Float64(u[i])
            maxdiff = ifelse(maxdiff < d, d, maxdiff)
            u[i] = mvdamp(newu, u[i], λ) # TODO: merge damp and fdamp
        end
        addfield!(m, h, u)
        return maxdiff, U, M
    end

    global function theta_propagate_exact(vh::Vec, ξ::Vec, κ::Int = 0)
        N = length(vh)
        leftC, rightC = getC(N)

        leftC[1][1] = (1-ξ[1]*vh[1])/2
        leftC[1][2] = (1+ξ[1]*vh[1])/2
        for i = 2:N
            lC0, lC = leftC[i-1], leftC[i]
            hi = ξ[i] * vh[i]
            hm, hp = (1-hi)/2, (1+hi)/2
            lC[1] = lC0[1] * hm
            for j = 2:i
                lC[j] = lC0[j-1] * hp + lC0[j] * hm
            end
            lC[end] = lC0[end] * hp
        end

        rightC[end][1] = (1-ξ[end]*vh[end])/2
        rightC[end][2] = (1+ξ[end]*vh[end])/2
        for i = (N-1):-1:1
            rC0, rC = rightC[i+1], rightC[i]
            hi = ξ[i] * vh[i]
            hm, hp = (1-hi)/2, (1+hi)/2
            rC[1] = rC0[1] * hm
            for j = 2:(N-i+1)
                rC[j] = rC0[j-1] * hp + rC0[j] * hm
            end
            rC[end] = rC0[end] * hp
        end

        @assert maximum(abs.(leftC[end] .- rightC[1])) ≤ 1e-10 (leftC[end], rightC[1])

        @assert isodd(N)
        z = (N+1) ÷ 2 + κ
        pm = sum(rightC[1][1:z])
        pp = sum(rightC[1][(z+1):end])

        return pp, pm, leftC, rightC
    end

    global function theta_node_update_exact!(m::MagVec{F}, M::F, ξ::Vec, u::MagVec{F}, U::F, params::Params, κ::Int) where {F<:Mag64}
        @extract params : λ=damping

        # @info "EXACT"


        N = length(m)
        h::MagVec{F} = geth(F, N)
        vh = getvh(N)

        @assert abs(κ) ≤ N ÷ 2

        subfield!(h, m, u)
        H = M ⊘ U

        @inbounds for i = 1:N
            vh[i] = h[i]
        end

        κ *= sign0(M) # robustness

        pp, pm, leftC, rightC = theta_propagate_exact(vh, ξ, κ)

        newU = Mag64(F, pp, pm)

        @assert isfinite(newU)

        maxdiff = 0.0
        U = damp(newU, U, λ)
        newM = H ⊗ U
        # @show M, H, U, newM
        M = newM

        @assert isfinite(newM) (H, U)

        z = (N+1) ÷ 2 + κ
        u1 = ones(1)

        @inbounds for i = 1:N
            ξi = ξ[i]
            @assert ξi^2 == 1

            lC = i > 1 ? leftC[i-1] : u1
            rC = i < N ? rightC[i+1] : u1

            pm = 0.0
            pz = 0.0
            pp = 0.0
            for j = 1:N
                p = 0.0
                for k = max(1,j+i-N):min(j,i)
                    p += lC[k] * rC[j-k+1]
                end
                if j < z
                    pm += p
                elseif j == z
                    pz = p
                else
                    pp += p
                end
            end

            mp = convert(F, clamp(pp + ξi * pz - pm, -1.0, 1.0))
            mm = convert(F, clamp(pp - ξi * pz - pm, -1.0, 1.0))
            newu = exactmix(H, mp, mm)
            if κ == N ÷ 2
                newu = M
                # newu ≈ M || @show newu, M, mp, mm, H
                # @assert newu ≈ M
            end

            d = abs(newu - u[i])
            maxdiff = ifelse(maxdiff < d, d, maxdiff)
            u[i] = damp(newu, u[i], λ)
            # @show i, newu, u[i]
            @assert isfinite(u[i]) (u[i],)
        end
        # @show m, h, u
        addfield!(m, h, u)
        # @show m
        return maxdiff, U, M
    end

    global function free_energy_theta(m::MagVec{F}, M::F, ξ::Vec, u::MagVec{F}, U::F) where {F<:Mag64}
        N = length(m)
        h::MagVec{F} = geth(F, N)

        subfield!(h, m, u)
        H = M ⊘ U

        f = -log1pxy(H, U)
        @assert isfinite(f)

        for i = 1:N
            f += log1pxy(h[i], u[i])
        end
        return f
    end

    function free_energy_theta(m::MVVec, M::F, ξ::Vec, u::MVVec, U::F, params::Params) where {F<:Mag64}
        N = length(m)
        h::MVVec = geth(MeanVar, N)

        subfield!(h, m, u)
        H = M ⊘ U

        f = -log1pxy(H, U)
        @assert isfinite(f)

        for i = 1:N
            f += logz(m[i]) - logz(h[i])
            # f += loghu(h[i], u[i])
        end
        return f
    end

    global function free_cache!()
        for d in [hsT, hsP, hsA, vhs, vhvs, Cs]
            empty!(d)
        end
    end
end

function entro_node_update(m::MeanVar, u::MeanVar, params::Params)
    @extract params : λ=damping r γ χst

    h = m ⊘ u
    if r == Inf
        μin, vin = float(h)
        lγ = min(γ, 1e20) # dealing with infinity somehow...
        newu = MeanVar(μin * lγ, lγ)
    else
        newu = (((h * γ) ↑ r) ⊗ χst) * γ
    end

    diff = max(abs(newu.λμ - u.λμ),
               abs(newu.λ - u.λ))
    newu = mvdamp(newu, u, λ)
    newm = h ⊗ newu

    return diff, newu, newm
end

function iterate!(messages::Messages{F}, patterns::Patterns, params::Params) where {F<:Mag64}
    @extract messages : N M K ux mw mτ1 uw Uτ1 mτ2 uτ1 Uτ2
    @extract patterns : X output
    @extract params   : accuracy1 accuracy2 χ χst κ
    maxdiff = 0.0
    tnu1! = accuracy1 == :accurate ? theta_node_update_accurate! :
            accuracy1 == :approx ? theta_node_update_approx! :
            error("accuracy1 must be one of :accurate or :approx (was given $accuracy2)")
    tnu2! = accuracy2 == :exact ? theta_node_update_exact! :
            accuracy2 == :accurate ? theta_node_update_accurate! :
            accuracy2 == :approx ? theta_node_update_approx! :
            error("accuracy2 must be one of :exact, :accurate, :approx (was given $accuracy2)")
    Nk = N ÷ K
    for a = randperm(M + N)
        if a ≤ M
            # @info "a=$a"
            ξ = X[a]
            i0 = 1
            for k = 1:K
                ξk = ξ[i0:(i0+Nk-1)]
                diff, Uτ1[a][k], mτ1[a][k] = tnu1!(mw[k], mτ1[a][k], ξk, uw[a][k], Uτ1[a][k], params)
                maxdiff = max(maxdiff, diff)
                i0 += Nk
            end
            diff, Uτ2[a], mτ2[a] = tnu2!(mτ1[a], mτ2[a], ones(K), uτ1[a], Uτ2[a], params, κ)
            # @show a,mτ2[a]
            # @show a,uτ1[a]
            # @show a,mτ1[a]
            maxdiff = max(maxdiff, diff)
        else
            # iszero(params.γ) && continue
            j = a - M
            k = (j-1) ÷ Nk + 1
            i = (j-1) % Nk + 1

            diff, ux[k][i], mw[k][i] = entro_node_update(mw[k][i], ux[k][i], params)
            maxdiff = max(diff, maxdiff)
        end
    end
    q = compute_q(messages)
    newχ = MeanVar(χ.λ * q)
    for k = 1:K, i = 1:Nk
        mw[k][i] = mw[k][i] ⊘ χ ⊗ newχ
    end
    maxdiff = max(maxdiff, abs(q - 1))
    params.χ = newχ

    q̃ = compute_q̃(messages, params)
    newχst = MeanVar(χst.λ + q̃ - 1)
    maxdiff = max(maxdiff, abs(q̃ - 1))
    params.χst = newχst

    return maxdiff
end

function converge!(messages::Messages, patterns::Patterns, params::Params)
    @extract params : ϵ max_iters λ₀=damping quiet

    λ = λ₀
    ok = false
    strl = 0
    t = @elapsed for it = 1:max_iters
        diff = iterate!(messages, patterns, params)

        if !quiet
            str = "[it=$it Δ=$diff λ=$λ]"
            print("\r", " "^strl, "\r", str)
            strl = length(str)
            #println(str)
            flush(stdout)
            strl = length(str)
        end
        if diff < ϵ
            ok = true
            quiet || println("\nok")
            break
        end
    end
    if !quiet
        ok || println("\nfailed")
        println("elapsed time = $t seconds")
    end
    return ok
end

function test(messages::Messages, patterns::Patterns)
    @extract messages : N K mw
    @extract patterns : X output
    Nk = N ÷ K
    μs = [Vector{Float64}(undef, Nk) for k = 1:K]
    vs = [Vector{Float64}(undef, Nk) for k = 1:K]
    for k = 1:K, i = 1:Nk
        μs[k][i], vs[k][i] = float(mw[k][i])
    end
    return test(μs, vs, N, K, patterns)
end

function testx(messages::Messages, patterns::Patterns, params::Params)
    @extract messages : N K mw ux
    @extract patterns : X output
    @extract params   : r γ χst
    Nk = N ÷ K
    μs = [Vector{Float64}(undef, Nk) for k = 1:K]
    vs = [Vector{Float64}(undef, Nk) for k = 1:K]
    y = r + 1
    for k = 1:K, i = 1:Nk
        hkix = mw[k][i] ⊘ ux[k][i]
        mx = ((hkix * γ) ↑ y) ⊗ χst
        μs[k][i], vs[k][i] = float(mx)
    end
    return test(μs, vs, N, K, patterns)
end

function test(μs::Vec2, vs::Vec2, N::Int, K::Int, patterns::Patterns)
    @extract patterns : X output
    Nk = N ÷ K
    svs = map(sum, vs)
    err = 0
    τ = zeros(K)
    for (ξ, o) in zip(X, output)
        s = 0.0
        for k = 1:K
            rng = ((k-1) * Nk + 1):(k*Nk)
            τ[k] = erf((μs[k] ⋅ (@view ξ[rng])) / √(2*svs[k]))
        end
        pp, pm, _, _ = theta_propagate_exact(τ, ones(K))
        err += (sign(pp-pm) ≠ o)
    end
    return err
end

function nonbayes_test(messages::Messages, patterns::Patterns)
    @extract messages : N K mw
    @extract patterns : X output
    Nk = N ÷ K
    μs = [Vector{Float64}(undef, Nk) for k = 1:K]
    for k = 1:K, i = 1:Nk
        μs[k][i], _ = float(mw[k][i])
    end
    return nonbayes_test(μs, N, K, patterns)
end

function nonbayes_testx(messages::Messages, patterns::Patterns, params::Params)
    @extract messages : N K mw ux
    @extract patterns : X output
    @extract params   : r γ χst
    Nk = N ÷ K
    μs = [Vector{Float64}(undef, Nk) for k = 1:K]
    y = r + 1
    for k = 1:K, i = 1:Nk
        hkix = mw[k][i] ⊘ ux[k][i]
        mx = ((hkix * γ) ↑ y) ⊗ χst
        μs[k][i], _ = float(mx)
    end
    return nonbayes_test(μs, N, K, patterns)
end

# simpler and faster...
function nonbayes_test(μs::Vec2, N::Int, K::Int, patterns::Patterns)
    @extract patterns : X output
    Nk = N ÷ K
    err = 0
    for (ξ, o) in zip(X, output)
        s = 0.0
        for k = 1:K
            rng = ((k-1) * Nk + 1):(k*Nk)
            s += sign(μs[k] ⋅ (@view ξ[rng]))
        end
        err += (sign(s) ≠ o)
    end
    return err
end

gen_error_empirical(messages::Messages, patterns::Patterns, α::Real = 5.0) =
    gen_error_empirical(messages, patterns.teacher, α)

gen_error_empiricalx(messages::Messages, patterns::Patterns, params::Params, α::Real = 5.0) =
    gen_error_empirical(messages, patterns.teacher, α, test_func=(m,p)->testx(m,p,params))

nonbayes_gen_error_empirical(messages::Messages, patterns::Patterns, α::Real = 5.0) =
    gen_error_empirical(messages, patterns.teacher, α, test_func=nonbayes_test)

nonbayes_gen_error_empiricalx(messages::Messages, patterns::Patterns, params::Params, α::Real = 5.0) =
    gen_error_empirical(messages, patterns.teacher, α, test_func=(m,p)->nonbayes_testx(m,p,params))

function gen_error_empirical(messages::Messages, tw::Vec2, α::Real = 5.0; test_func=test)
    @extract messages : N K
    length(tw) == K || throw(ArgumentError("Incompatible messages/teacher, different K: $K vs $(length(tw))"))
    N % K == 0 || throw(ArgumentError("N must be divisible by K: N=$N K=$K"))
    Nk = N ÷ K
    all(w -> length(w) == Nk, tw) || throw(ArgumentError("invalid teacher length, expected $Nk, given: $(sort!(unique(length.(tw))))"))
    α > 0 || throw(ArgumentError("invalid α, must be positive, given: $α"))
    M = round(Int, N * α)
    testpatterns = Patterns((N,M), teacher=tw)
    gerr = test_func(messages, testpatterns) / M
    return gerr
end

function free_energy(messages::Messages{F}, patterns::Patterns, params::Params) where {F<:Mag64}
    @extract messages : M N K ux mw mτ1 uw Uτ1 mτ2 uτ1 Uτ2
    @extract patterns : X output
    @extract params   : χst γ r

    f = 0.0

    for a = 1:M
        ξ = X[a]
        for k = 1:K
            f += free_energy_theta(mw[k], mτ1[a][k], ξ, uw[a][k], Uτ1[a][k], params)
        end
        f += free_energy_theta(mτ1[a], mτ2[a], ones(K), uτ1[a], Uτ2[a])
    end

    Nk = N ÷ K
    for k = 1:K, i = 1:Nk
        f -= logz(mw[k][i])
        # the above is the same as:
        # f -= logz([params.χ; ux[k][i]; [uw[a][k][i] for a in 1:M]])

        uxki = ux[k][i]
        hkix = mw[k][i] ⊘ uxki
        ukix = hkix * γ
        hxki = (ukix ↑ r) ⊗ χst
        μkix, vkix = float(hkix)
        μxki, vxki = float(hxki)

        # -ΔFₐ
        @extract hkix : λμ λ
        y = r + 1
        a = y * γ + χst.λ
        b = a * λ + γ * χst.λ
        # y == 1 && @assert (ux[k][i].λμ == 0 && ux[k][i].λ ≈ γ * χst.λ / (γ + χst.λ))
        # y == 1 && @assert -1/2 * (γ * λμ^2 * χst.λ) / (λ * b) + 1/2 * log(λ) - r / 2y * log(γ + λ) + 1 / 2y * log(a) - 1 / 2y * log(b) ≈ loghu(hkix, uxki)

        f -= -1/2 * (γ * λμ^2 * χst.λ) / (λ * b) + 1/2 * log(λ) - r / 2y * log(γ + λ) + 1 / 2y * log(a) - 1 / 2y * log(b)

        # energy term (moved to compute_Ex)
        # f += -1/2 * γ * ((a * λμ^2 * χst.λ) / b^2 + (χst.λ + r * γ * λ / (γ + λ)) / b)

        # # -ΔFₐ
        # f -= -1/2 * γ * (μxki + μkix)^2 / (1 + (vkix + vxki) * γ) - 1/2 * log(1 + (vkix + vxki) * γ)
        # -ΔFᵢₐ
        f += loghu(hkix, uxki)
        # # -ΔFₐₓ
        # f += loghu(hxki, ukix)
        #
        # # subtract the W̃ entropy
        # mx = (ukix ↑ y) ⊗ χst
        # f -= logz(mx) / y
        # f -= 1/2 * χst.λ / y
        # f += log(√(2π * ℯ)) / y

        # @assert isfinite(f) "D"
        # isfinite(f) || @warn "non-finite f"
    end

    return f / N
end

function compute_S(messages::Messages{F}, params::Params) where {F<:Mag64}
    @extract messages : N K ux mw
    @extract params   : r γ χst

    Nk = N ÷ K
    S = 0.0
    for k = 1:K, i = 1:Nk
        hkix = mw[k][i] ⊘ ux[k][i]
        hxki = ((hkix * γ) ↑ r) ⊗ χst
        S += overlap(hkix, hxki, γ)
    end
    return S / N
end

function compute_distx(messages::Messages{F}, params::Params) where {F<:Mag64}
    @extract messages : N K ux mw
    @extract params   : r γ χst

    Nk = N ÷ K
    d = 0.0
    for k = 1:K, i = 1:Nk
        hkix = mw[k][i] ⊘ ux[k][i]
        hxki = ((hkix * γ) ↑ r) ⊗ χst
        d += squareddiff(hkix, hxki, γ)
    end
    return d / 2N
end


# mean of (x - wp)^2 / 2
# we pass wp even though it's basically stored in up just for the γ==0 case
function compute_distp(messages::Messages{F}, wp::Vector) where {F<:Mag64}
    @extract messages : N K up mw
    z = norm(wp) / √N
    Nk = N ÷ K
    d = 0.0
    for k = 1:K, i = 1:Nk
        h = mw[k][i] ⊘ up[k][i]
        μ, v = float(h)
        _, vp = float(up[k][i])
        μp = wp[(k-1)*Nk + i] / z
        if vp == Inf
            d += (v + (μ - μp)^2) / 2
        else
            d += vp / 2 * (v / (v + vp) + vp * (μ - μp)^2 / (v + vp)^2)
        end
    end
    return d / N
end

function compute_Sp(messages::Messages{F}, wp::Vector) where {F<:Mag64}
    @extract messages : N K up mw
    z = norm(wp) / √N
    Nk = N ÷ K
    S = 0.0
    for k = 1:K, i = 1:Nk
        h = mw[k][i] ⊘ up[k][i]
        μ, v = float(h)
        _, vp = float(up[k][i])
        μp = wp[(k-1)*Nk + i] / z
        if vp == Inf
            S += μ * μp
        else
            S += μp * (vp * μ + v * μp) / (v + vp)
        end
    end
    return S / N
end

function compute_q̃(messages::Messages{F}, params::Params) where {F<:Mag64}
    @extract messages : N K ux mw
    @extract params   : r γ χst
    Nk = N ÷ K
    y = r + 1
    q̃ = 0.0
    for k = 1:K, i = 1:Nk
        hkix = mw[k][i] ⊘ ux[k][i]
        mx = ((hkix * γ) ↑ y) ⊗ χst
        μx, vx = float(mx)
        q̃ += vx + μx^2
    end
    return q̃ / N
end

function compute_q(messages::Messages)
    @extract messages : M N K mw
    Nk = N ÷ K
    q = 0.0
    for k = 1:K, i = 1:Nk
        μ, v = float(mw[k][i])
        q += v + μ^2
    end
    return q / N
end

function compute_Ex(messages::Messages, params::Params)
    @extract messages : M N K mw ux up
    @extract params : χ χst γ r
    Nk = N ÷ K
    Ex = 0.0
    for k = 1:K, i = 1:Nk
        μ, v = float(mw[k][i])
        # u = χ ⊗ ux[k][i] ⊗ up[k][i]
        u = χ ⊗ up[k][i]
        Ex += 1/2 * u.λ * (v + μ^2) - u.λμ * μ

        hkix = mw[k][i] ⊘ ux[k][i]
        @extract hkix : λμ λ
        a = (r + 1) * γ + χst.λ
        b = a * λ + γ * χst.λ
        Ex += 1/2 * γ * ((a * λμ^2 * χst.λ) / b^2 + (χst.λ + r * γ * λ / (γ + λ)) / b)
    end
    return Ex / N
end

# NOTE: this is the same as 1-d*(2-d) where d=compute_distx
function compute_qab(messages::Messages, params::Params)
    @extract messages : M N K mw ux up
    @extract params : χst r γ
    Nk = N ÷ K
    qab = 0.0
    for k = 1:K, i = 1:Nk
        hkix = mw[k][i] ⊘ ux[k][i]
        hx = ((hkix * γ) ↑ (r-1)) ⊗ χst
        qab += repl_overlap(hkix, hx, γ)
    end
    return qab / N
end

function compute_W²(messages::Messages)
    @extract messages : M N K mw
    Nk = N ÷ K
    W² = 0.0
    for k = 1:K, i = 1:Nk
        μ, _ = float(mw[k][i])
        W² += μ^2
    end
    return W² / N
end

function sample_W²(messages::Messages; samples = 1_000)
    @extract messages : M N K mw
    Nk = N ÷ K
    W² = 0.0
    for k = 1:K, i = 1:Nk
        μ, v = float(mw[k][i])
        W² += sum((μ .+ √v .* randn(samples)).^2)
    end
    return W² / (N * samples)
end

function compute_W̃²(messages::Messages{F}, params::Params) where {F<:Mag64}
    @extract messages : N K ux mw
    @extract params   : r γ χst
    Nk = N ÷ K
    y = r + 1
    W̃² = 0.0
    for k = 1:K, i = 1:Nk
        hkix = mw[k][i] ⊘ ux[k][i]
        mx = ((hkix * γ) ↑ y) ⊗ χst
        μx, _ = float(mx)
        W̃² += μx^2
    end
    return W̃² / N
end

"""
    FocusingProtocol

Abstract type representing a protocol for the focusing procedure, i.e. a way to produce
successive values for the quantities `γ`, `y` and `β`. Currently, however, only `β=Inf`
is supported. To be provided as an argument to [`focusingBP`](@ref).

Available protocols are: [`StandardReinforcement`](@ref), [`Scoping`](@ref), [`PseudoReinforcement`](@ref) and
[`FreeScoping`](@ref).
"""
abstract type FocusingProtocol end

struct StandardReinforcement <: FocusingProtocol
    r::AbstractRange{Float64}
    StandardReinforcement(r::AbstractRange{T}) where {T<:Real} = new(r)
end

@doc """
    StandardReinforcement(r::AbstractRange) <: FocusingProtocol

Standard reinforcement protocol, returns `γ=Inf` and `y=1/(1-x)`, where `x` is taken from the given range `r`.
""" StandardReinforcement(r::AbstractRange)

"""
    StandardReinforcement(dr::Float64) <: FocusingProtocol

Shorthand for [`StandardReinforcement`](@ref)`(0:dr:(1-dr))`.
"""
StandardReinforcement(dr::Float64) = StandardReinforcement(0.0:dr:(1-dr))

function Base.iterate(s::StandardReinforcement, i = nothing)
    n = i ≡ nothing ? iterate(s.r) : iterate(s.r, i)
    n ≡ nothing && return nothing
    return (Inf, 1/(1-n[1]), 0, Inf), n[2] # FIXME
end

"""
    Scoping(γr::AbstractRange, y) <: FocusingProtocol

Focusing protocol with fixed `y` and a varying `γ` taken from the given `γr` range.
"""
struct Scoping <: FocusingProtocol
    γr::AbstractRange{Float64}
    y::Float64
    Scoping(γr::AbstractRange, y) = new(γr, y)
end

function Base.iterate(s::Scoping, i = nothing)
    n = i ≡ nothing ? iterate(s.γr) : iterate(s.γr, i)
    n ≡ nothing && return nothing
    return (n[1], s.y, 0, Inf), n[2]
end
Base.length(s::Scoping) = length(s.γr)


struct PseudoReinforcement <: FocusingProtocol
    r::Vector{Float64}
    x::Float64
    PseudoReinforcement(r::AbstractRange{T}...; x::Real=0.5) where {T<:Real} = new(vcat(map(collect, r)...), x)
end
@doc """
    PseudoReinforcement(r::AbstractRange...; x=0.5) <: FocusingProtocol

A focusing protocol in which both `γ` and `y` are progressively increased, according to
the formulas

```julia
γ = atanh(ρ^x)
y = 1+ρ^(1-2x)/(1-ρ)
```

where `ρ` is taken from the given range(s) `r`. With `x=0`, this is basically the same as
[`StandardReinforcement`](@ref).
""" PseudoReinforcement(r::AbstractRange...)

"""
    PseudoReinforcement(dr::Float64; x=0.5) <: FocusingProtocol

Shorthand for [`PseudoReinforcement`](@ref)`(0:dr:(1-dr); x=x)`.
""" PseudoReinforcement(dr::Float64; x::Real=0.5) = PseudoReinforcement(0.0:dr:(1-dr), x=x)

function Base.iterate(s::PseudoReinforcement, i = nothing)
    i == -1 && return nothing
    n = i ≡ nothing ? iterate(s.r) : iterate(s.r, i)
    n ≡ nothing && return (Inf, Inf, 0, Inf), -1
    x = s.x
    ρ = n[1]
    # some special cases just to avoid possible 0^0
    if x == 0.5
        return (atanh(√ρ), (2-ρ)/(1-ρ), 0, Inf), n[2]
    elseif x == 0
        return (Inf, 1/(1-ρ), 0, Inf), n[2]
    else
        return (atanh(ρ^x), 1+ρ^(1-2x)/(1-ρ), 0, Inf), n[2]
    end
end
Base.length(s::PseudoReinforcement) = length(s.r) + 1

"""
    FreeScoping(list::Vector{NTuple{2,Float64}}) <: FocusingProtocol

A focusing protocol which just returns the values of `(γ,y)` from the given `list`.

Example:

```julia
FreeScoping([(1/(1-x), (2-x)/(1-x)) for x = 0:0.01:0.99])
```
"""
struct FreeScoping <: FocusingProtocol
    list::Vector{Tuple{Float64,Float64,Int,Float64}}
    FreeScoping(list::Vector{Tuple{Float64,Float64,Int,Float64}}) = new(list)
end
FreeScoping(list::Vector{NTuple{2,Float64}}) = FreeScoping(Tuple{Float64,Float64,Int,Float64}[(γ,y,0,Inf) for (γ,y) in list])
FreeScoping(list::Vector{NTuple{3,Float64}}) = FreeScoping(Tuple{Float64,Float64,Int,Float64}[(γ,y,0,β) for (γ,y,β) in list])
FreeScoping(list::Vector{Tuple{Float64,Float64,Int}}) = FreeScoping(Tuple{Float64,Float64,Int,Float64}[(γ,y,κ,Inf) for (γ,y,κ) in list])

Base.iterate(s::FreeScoping) = iterate(s.list)
Base.iterate(s::FreeScoping, i) = iterate(s.list, i)
Base.length(s::FreeScoping) = length(s.list)

"""
    focusingBP(N, K, patternspec; keywords...)

Run the Focusing Belief Propagation algorithm on a fully-connected committee machine with binary weights.
`N` is the input (first layer) size, `K` the number of the hidden units (second layer size), and
`patternspec` specifies how to build the patterns for the training set. Note that with the defult
settings `K` must be odd (see notes for the `accuracy1` and `accuracy2` arguments below).

Possible values of `patternspec` are:

* a `Float64` number: this is interpreted as the `α` parameter, and `M = α*N*K` random ±1 patterns are generated.
* a `Tuple` with `Vector{Vector{Float64}}` and a `Vector{Float64}`: these are the inputs and associated desired outputs.
* a string: the patterns are read from a file (one input pattern per line, entries separated by whitespace, outputs are
            assumed to be all 1); the file can be gzipped.
* a [`Patterns`](@ref) object (which could be the output of a previous run of the function).

*Note*: all inputs and outputs must be ∈ {-1,1}.

The keyword arguments are:

* `max_iters` (default = `1000`): maximum number of BP iterations per step. If convergence is not achieved in this many iterations,
                                  the algorithm proceeds to the next step.
* `max_steps` (default = `typemax(Int)`): maximum number of focusing steps.
* `seed` (default = `1`): random seed.
* `damping` (default = `0`): BP damping parameter (between `0` and `1`; `0` = no damping).
* `quiet` (default = `false`): whether to print on screen
* `accuracy1` (default = `:accurate`): accuracy of the messages computation at the hidden units level. Possible values are:
                                       `:accurate` (Gaussian approximation, good for large `N`, works in `O(N)` time),
                                       `:exact` (no approximation, uses convolutions, good for small `N`, works in `O(N³)` time, requires
                                       `N` to be odd),
                                       `:approx` (a TAP-like approximation).
* `accuracy2` (default = `:exact`): accuracy of the messages computation at the output node level.
                                    See `accuracy1` (and think of `K` instead of `N`).
* `fprotocol` (default = `StandardReinforcement(1e-2)`): focusing protocol specification. See [`FocusingProtocol`](@ref).
* `ϵ` (default = `1e-3`): convergence criterion: BP is assumed to have converged when the difference between messages in two successive
                          iterations is smaller than this value. Reduce it (e.g. to 1e-6) for more precise results, while increasing
                          `max_iters`.
* `messfmt` (default = `:tanh`): internal storage format for messages: it can be either `:tanh` or `:plain`; `:tanh` is much more
                                 precise but slower.
* `initmess` (default = `nothing`): how to initialize the messages. If `nothing`, they are initialized randomly; If a string is given,
                                    they are read from a file (see also [`read_messages`](@ref) and [`write_messages`](@ref)). If a
                                    `Messages` object is given (e.g. one returned from an earlier run of the function, or from
                                    [`read_messages`](@ref)) it is used (and overwritten).
* `outatzero` (default = `true`): if `true`, the algorithm exits as soon as a solution to the learning problem is found, without waiting
                                  for the focusing protocol to terminate.
* `writeoutfile` (default = `:auto`): whether to write results on an output file. Can be `:never`, `:always` or `:auto`. The latter means
                                      that a file is written when `outatzero` is set to `false` and only when BP converges. It can make sense
                                      setting this to `:always` even when `outfile` is `nothing` to force the computation of the local
                                      entropy and other thermodynamic quantities.
* `outfile` (default = `nothing`): the output file name. `nothing` means no output file is written. An empty string means using a default
                                   file name of the form: `"results_BPCR_N\$(N)_K\$(K)_M\$(M)_s\$(seed).txt"`.
* `outmessfiletmpl` (default = `nothing`): template file name for writing the messages at each focusing step. The file name should include a
                                           substring `"%gamma%"` which will be substituted with the value of the `γ` parameter at each step.
                                           If `nothing`, it is not used. If empty, the default will be used:
                                           `"messages_BPCR_N\$(N)_K\$(K)_M\$(M)_g%gamma%_s\$(seed).txt.gz"`.
                                           *Note*: this can produce a lot of fairly large files!.

The function returns three objects: the number of training errors, the messages and the patterns. The last two can be used as inputs to
successive runs of the algorithms, as the `initmessages` keyword argument and the `patternspec` argument, respectively.

Example of a run which solves a problem with `N * K = 1605` synapses with `K = 5` at `α = 0.3`:
```
julia> errs, messages, patterns = focusingBP(321, 5, 0.3, seed=135, max_iters=1, damping=0.5);
```
"""
function focusingBP(N::Integer, K::Integer,
                    initpatt::Union{AbstractString,Tuple{Vec2,Vec},Real,Patterns};

                    max_iters::Integer = 1000,
                    max_steps::Integer = typemax(Int),
                    seed::Integer = 1,
                    damping::Real = 0.0,
                    quiet::Bool = false,
                    accuracy1::Symbol = :accurate,
                    accuracy2::Symbol = :exact,
                    fprotocol::FocusingProtocol = StandardReinforcement(1e-2),
                    ϵ::Real = 1e-3,
                    messfmt::Symbol = :tanh,
                    initmess::Union{Messages,Nothing,AbstractString} = nothing,
                    outatzero::Bool = true,
                    writeoutfile::Symbol = :auto, # note: ∈ [:auto, :always, :never]; auto => !outatzero && converged
                    outfile::Union{AbstractString,Nothing} = nothing, # note: "" => default, nothing => no output
                    outmessfiletmpl::Union{AbstractString,Nothing} = nothing, # note: same as outfile
                    rand_fact::Float64 = 0.0
                   )

    Random.seed!(seed)

    N > 0 || throw(ArgumentError("N must be positive; given: $N"))
    K > 0 || throw(ArgumentError("K must be positive; given: $K"))
    N % K == 0 || throw(ArgumentError("N must be divisible by K, given: N=$N K=$K"))

    writeoutfile ∈ [:auto, :always, :never] || error("invalide writeoutfile, expected one of :auto, :always, :never; given: $writeoutfile")
    max_iters ≥ 0 || throw(ArgumentError("max_iters must be non-negative; given: $max_iters"))
    max_steps ≥ 0 || throw(ArgumentError("max_steps must be non-negative; given: $max_steps"))
    0 ≤ damping < 1 || throw(ArgumentError("damping must be ∈ [0,1); given: $damping"))
    accuracy1 ∈ [:accurate, :approx] || error("accuracy1 must be one of :accurate, :approx; given: $accuracy1")
    accuracy2 ∈ [:exact, :accurate, :approx] || error("accuracy2 must be one of :exact, :accurate, :approx; given: $accuracy2")

    accuracy2 == :exact && iseven(K) && throw(ArgumentError("when accuracy2==:exact K must be odd, given: $K"))

    messfmt ∈ [:tanh, :plain] || throw(ArgumentError("invalid messfmt, should be :tanh or :plain; given: $messfmt"))
    F = messfmt == :tanh ? MagT64 : MagP64

    if isa(initpatt, Real)
        initpatt ≥ 0 || throw(ArgumentError("invalide negative initpatt; given: $initpatt"))
        initpatt = (N, round(Int, N * initpatt))
    end

    patterns = Patterns(initpatt)

    M = patterns.M

    messages::Messages = initmess ≡ nothing ? Messages(F, M, N, K, x = rand_fact) :
                         isa(initmess, AbstractString) ? read_messages(initmess, F) :
                         initmess

    messages.N == N || throw(ArgumentError("wrong messages size, expected N=$N; given: $(messages.N)"))
    messages.K == K || throw(ArgumentError("wrong messages size, expected K=$K; given: $(messages.K)"))
    messages.M == M || throw(ArgumentError("wrong messages size, expected M=$M; given: $(messages.M)"))
    F = eltype(messages)

    params = Params(damping, ϵ, NaN, max_iters, accuracy1, accuracy2, 1.0, 1.0, 0.0, 0.0, quiet, 0)

    outfile == "" && (outfile = "results_fBP_N$(N)_K$(K)_M$(M)_s$(seed).txt")
    # outmessfiletmpl == "" && (outmessfiletmpl = "messages_fBP_N$(N)_K$(K)_M$(M)_g%gamma%_s$(seed).txt.gz")
    lockfile = "bptree.lock"
    if outfile ≢ nothing && writeoutfile ∈ [:always, :auto]
        !quiet && println("writing outfile $outfile")
        exclusive(lockfile) do
            !isfile(outfile) && open(outfile, "w") do f
                gerr_header = patterns.teacher ≢ nothing ? " 16=gE 17=⟨gE⟩ 18=gẼ 19=⟨gẼ⟩" : ""
                println(f, "#1=ok 2=γ 3=y 4=β 5=dist 6=𝓢ᵢₙₜ 7=q 8=q̃ 9=S 10=qab 11=βF 12=E 13=Ẽ 14=⟨E⟩ 15=⟨Ẽ⟩", gerr_header)
            end
        end
    end

    ok = true
    errs = nonbayes_test(messages, patterns)
    if initmess ≢ nothing
        !quiet && println("initial errors = $errs")

        if outatzero && errs == 0
            free_cache!()
            return errs, messages, patterns, params
        end
    end

    it = 1
    for (γ,y,κ,β) in fprotocol
        isfinite(β) && error("finite β not yet supported; given: $β")
        params.γ = γ
        params.r = y - 1
        params.β = β
        abs(κ) ≤ (K÷2) || throw(ArgumentError("invalid robustness κ, must be |κ|≤K÷2; given: $κ"))
        params.κ = κ
        set_outfields!(messages, patterns.output, params.β)
        ok = converge!(messages, patterns, params)
        berrs = test(messages, patterns)
        berrsx = testx(messages, patterns, params)
        errs = nonbayes_test(messages, patterns)
        errsx = nonbayes_testx(messages, patterns, params)
        if patterns.teacher ≢ nothing
            gen_berr = gen_error_empirical(messages, patterns, 5.0)
            gen_berrx = gen_error_empiricalx(messages, patterns, params, 5.0)
            gen_err = nonbayes_gen_error_empirical(messages, patterns, 5.0)
            gen_errx = nonbayes_gen_error_empiricalx(messages, patterns, params, 5.0)
            gen_err_fstring = " $gen_err $gen_berr $gen_errx $gen_berrx"
            gen_err_string = " gE=$gen_err ⟨gE⟩=$gen_berr gẼ=$gen_errx ⟨gẼ⟩=$gen_berrx"
        else
            gen_err_fstring = ""
            gen_err_string = ""
        end


        if writeoutfile == :always || (writeoutfile == :auto && !outatzero)
            S = compute_S(messages, params)
            q = compute_q(messages)
            q̃ = compute_q̃(messages, params)
            Ex = compute_Ex(messages, params)
            # dist = (q + q̃ - 2S) / 2
            dist = compute_distx(messages, params)
            qab = compute_qab(messages, params)
            # @show qab, ((dist-1)^2 * y - 1) / (y - 1), (dist-1)^2
            # @show dist, 1 - √(qab + (1 - qab) / y), 1 - √qab
            βF = free_energy(messages, patterns, params)
            Σint = -βF + Ex - (1/2 + log(√(2π)))
            # Σint = -βF + params.χ/2 - γ * dist - (1/2 + log(√(2π)))
            W² = compute_W²(messages)
            W̃² = compute_W̃²(messages, params)
            # mW² = sample_W²(messages, samples=1_000)

            !quiet && println("step=$it γ=$γ y=$y κ=$κ β=$β (ok=$ok)")
            !quiet && println("      βF=$βF Σᵢ=$Σint q=$q q̃=$q̃ W²=$W² W̃²=$W̃²")
            !quiet && println("      S=$S d=$dist qab=$qab")
            !quiet && println("      E=$errs Ẽ=$errsx ⟨E⟩=$berrs ⟨Ẽ⟩=$berrsx", gen_err_string)
            (ok || writeoutfile == :always) && outfile ≢ nothing && exclusive(lockfile) do
                open(outfile, "a") do f
                    println(f, "$ok $γ $y $β $dist $Σint $q $q̃ $S $qab $βF $errs $errsx $berrs $berrsx", gen_err_fstring)
                end
            end
            # if outmessfiletmpl ≢ nothing
            #     outmessfile = replace(outmessfiletmpl, "%gamma%" => γ)
            #     write_messages(outmessfile, messages)
            # end
        else
            !quiet && println("it=$it β=$β (ok=$ok) E=$errs Ẽ=$errsx ⟨E⟩=$berrs ⟨E⟩=$berrsx", gen_err_string)
        end
        if outatzero && errs == 0
            free_cache!()
            return 0, messages, patterns, params
        end
        it += 1
        it ≥ max_steps && break
    end
    # print_marginals(messages, params)
    # @show messages.uτ1
    free_cache!()
    return errs, messages, patterns, params
end

function weight_enum(N::Integer, K::Integer,
                     initpatt::Union{AbstractString,Tuple{Vec2,Vec},Real,Patterns},
                     wp::Vector{<:Real};
                     max_iters::Integer = 1000,
                     seed::Integer = 1,
                     damping::Real = 0.0,
                     quiet::Bool = false,
                     accuracy1::Symbol = :accurate,
                     accuracy2::Symbol = :exact,
                     γl = 0.0:1e-2:1.0,
                     ϵ::Real = 1e-3,
                     messfmt::Symbol = :tanh,
                     initmess::Union{Messages,Nothing,AbstractString} = nothing,
                     # writeoutfile::Symbol = :auto, # note: ∈ [:auto, :always, :never]; auto => !outatzero && converged
                     outfile::Union{AbstractString,Nothing} = nothing, # note: "" => default, nothing => no output
                     # outmessfiletmpl::Union{AbstractString,Nothing} = nothing) # note: same as outfile
                     rand_fact::Float64 = 0.0
                    )

    Random.seed!(seed)

    N > 0 || throw(ArgumentError("N must be positive; given: $N"))
    K > 0 || throw(ArgumentError("K must be positive; given: $K"))
    N % K == 0 || throw(ArgumentError("N must be divisible by K, given: N=$N K=$K"))

    # writeoutfile ∈ [:auto, :always, :never] || error("invalide writeoutfile, expected one of :auto, :always, :never; given: $writeoutfile")
    max_iters ≥ 0 || throw(ArgumentError("max_iters must be non-negative; given: $max_iters"))
    0 ≤ damping < 1 || throw(ArgumentError("damping must be ∈ [0,1); given: $damping"))
    accuracy1 ∈ [:accurate, :approx] || error("accuracy1 must be one of :accurate, :approx; given: $accuracy1")
    accuracy2 ∈ [:exact, :accurate, :approx] || error("accuracy2 must be one of :exact, :accurate, :approx; given: $accuracy2")

    accuracy2 == :exact && iseven(K) && throw(ArgumentError("when accuracy2==:exact K must be odd, given: $K"))

    messfmt ∈ [:tanh, :plain] || throw(ArgumentError("invalid messfmt, should be :tanh or :plain; given: $messfmt"))
    F = messfmt == :tanh ? MagT64 : MagP64

    if isa(initpatt, Real)
        initpatt ≥ 0 || throw(ArgumentError("invalide negative initpatt; given: $initpatt"))
        initpatt = (N, round(Int, N * initpatt))
    end

    patterns = Patterns(initpatt)

    M = patterns.M

    messages::Messages = initmess ≡ nothing ? Messages(F, M, N, K, x=rand_fact) :
                         isa(initmess, AbstractString) ? read_messages(initmess, F) :
                         initmess

    messages.N == N || throw(ArgumentError("wrong messages size, expected N=$N; given: $(messages.N)"))
    messages.K == K || throw(ArgumentError("wrong messages size, expected K=$K; given: $(messages.K)"))
    messages.M == M || throw(ArgumentError("wrong messages size, expected M=$M; given: $(messages.M)"))
    F = eltype(messages)

    κ = 0
    params = Params(damping, ϵ, NaN, max_iters, accuracy1, accuracy2, 1.0, 1.0, 0.0, 0.0, quiet, κ)

    outfile == "" && (outfile = "weight_enum_BPCR_N$(N)_K$(K)_M$(M)_s$(seed).txt")
    # outmessfiletmpl == "" && (outmessfiletmpl = "messages_BPCR_N$(N)_K$(K)_M$(M)_g%gamma%_s$(seed).txt.gz")
    lockfile = "bptree.lock"
    if outfile ≢ nothing
        !quiet && println("writing outfile $outfile")
        exclusive(lockfile) do
            !isfile(outfile) && open(outfile, "w") do f
                println(f, "#1=converged 2=γ 3=β 4=βF 5=𝓢ᵢₙₜ 6=q 7=W² 8=distp")
            end
        end
    end

    ok = true
    errs = nonbayes_test(messages, patterns)
    if initmess ≢ nothing
        !quiet && println("initial errors = $errs")
    end

    it = 1
    params.β = Inf
    params.r = 0
    set_outfields!(messages, patterns.output, params.β)
    for γ in γl
        params.γ = γ
        set_pfields!(messages, wp, params.γ)
        ok = converge!(messages, patterns, params)
        berrs = test(messages, patterns)
        errs = nonbayes_test(messages, patterns)

        q = compute_q(messages)
        Ex = compute_Ex(messages, params)
        distp = compute_distp(messages, wp)
        # Sp = compute_Sp(messages, wp)
        βF = free_energy(messages, patterns, params)
        Σint = -βF + Ex - (1/2 + log(√(2π)))
        W² = compute_W²(messages)

        !quiet && println("it=$it γ=$γ (ok=$ok)")
        !quiet && println("      βF=$βF Σᵢ=$Σint q=$q W²=$W² dp=$distp")
        !quiet && println("      E=$errs ⟨E⟩=$berrs")
        outfile ≢ nothing && exclusive(lockfile) do
            open(outfile, "a") do f
                println(f, "$ok $γ $(params.β) $βF $Σint $q $W² $distp")
            end
        end
        # if outmessfiletmpl ≢ nothing
        #     outmessfile = replace(outmessfiletmpl, "%gamma%" => γ)
        #     write_messages(outmessfile, messages)
        # end
        it += 1
    end
    # print_marginals(messages, params)
    # @show messages.uτ1
    free_cache!()
    return errs, messages, patterns
end

end # module
