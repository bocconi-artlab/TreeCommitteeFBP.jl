# TreeCommitteeFBP.jl

This package implements the Focusing Belief Propagation algorithm for
tree committee machines with continuous weights.

The code is written in [Julia](http://julialang.org).

The package is tested against Julia `1.6`

Warning: this is unpolished, research code, currently dumped as-is.

### Installation

To install the module, open Julia, enter in pkg mode by pressing the `]` key and enter:

```
pkg> clone https://gitlab.com/bocconi-artlab/TreeCommitteeFBP.jl
```

Dependencies will be installed automatically.

## Documentation

Not really. There are some docs but they still refer to the code that this one was derived from.
